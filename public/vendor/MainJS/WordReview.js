var deps;
deps = [];
var app = angular.module('app', ['ngSanitize']);

app.config(function ($interpolateProvider) {
    $interpolateProvider.startSymbol('//');
    $interpolateProvider.endSymbol('//');
});

app.service('ReviewService', function ($http) {
    this.GetDictionary = function () {
        return $http.get("/DictionaryBook/ajax/dicpublished").then(function (result) {
            return result.data;
        });
    }

    this.SearchWordTemp = function (dicId, typeId, searchText) {
        return $http.get("/DictionaryBook/ajax/searchwordtemp/" + dicId + "/" + typeId + "/" + searchText + "").then(function (result) {
            return result.data;
        });
    }

    this.ShowParticular = function (id) {
        return $http.get("/DictionaryBook/ajax/showParticular/" + id + "").then(function (result) {

            return result.data;
        });
    }


    this.SaveParticular = function (particular) {
        return $http.post("/DictionaryBook/ajax/saveParticular", particular).then(function (result) {
            return result.data;
        });
    }

    this.ReviewParticular = function (particular) {
        return $http.post("/DictionaryBook/review/particular").then(function (result) {
            return result.data;
        });
    }


});



app.controller("ReviewController", function ($scope, $http, ReviewService) {
    $scope.DictionaryId = 0;
    $scope.PublishStatus = 0;
    $scope.SearchText = "";
    $scope.Dictionaries = [];
    $scope.WordTemps = [];
    $scope.PublishStatus = "1";
    $scope.Particular = new Object();



    var init = function () {
        $scope.Dictionaries = [];
        GetDictionary();

    };
    init();

    function GetDictionary() {
        ReviewService.GetDictionary().then(function (data) {
            $scope.Dictionaries = data;
            if (data.length > 0) {
                $scope.DictionaryId = data[0].id;
            }
        }, function (err) {
        });
    };

    $scope.Search = function (DictionaryId, PublishStatus, SearchText) {

        if (angular.isUndefined(DictionaryId) || DictionaryId == '' || DictionaryId == null) {
            alert('Please select dictionary');
            return;
        }
        if (angular.isUndefined(PublishStatus) || PublishStatus == '' || PublishStatus == null) {
            alert('Please select type');
            return;
        }
        if (angular.isUndefined(SearchText) || SearchText == '' || SearchText == null) {
            alert('Please give a search text');
            return;
        }

        ReviewService.SearchWordTemp(DictionaryId, PublishStatus, SearchText).then(function (data) {
            if (data == null || data.length == 0) {
                $scope.WordTemps = [];
                $scope.Particular = new Object();
                alert('No data found');
            } else {
                $scope.WordTemps = data;
            }
        },
            function (err) {
                alert(err);
            });

    };


    $scope.ShowParticular = function (word) {
        // alert(word.id);

        ReviewService.ShowParticular(word.id).then(function (data) {
            $scope.Particular = data;

            if (data == null) {

            }
        },
            function (err) {
                alert(err);
            });
    };


    $scope.SaveParticular = function (entity) {

        if (confirm('Are you sure to publish the word?')) {
            $http({
                method: 'POST',
                url: "/DictionaryBook/ajax/saveParticular",
                data: $.param($scope.Particular),
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
            }).then(function (response) {
                alert('Word has been published successfully.');
                $scope.Particular = new Object();
                $scope.Search($scope.DictionaryId, $scope.PublishStatus, $scope.SearchText);
                // location.reload()
            }, function (response) {
                console.log(response);
                alert('This is embarassing. An error has occured. Please check the log for details');
            });

        }

    };


    $scope.ReviewParticular = function (entity) {

        if (confirm('Are you sure to review the word?')) {
            $http({
                method: 'POST',
                url: "/DictionaryBook/review/particular",
                data: $.param($scope.Particular),
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
            }).then(function (response) {
                alert('Word has been send to review.')
                location.reload()
            }, function (response) {
                alert('This is embarassing. An error has occured. Please check the log for details');
            });
        }

    };








});