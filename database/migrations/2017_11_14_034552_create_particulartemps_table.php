<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParticulartempsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('particulartemps', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('word_temp_id')->unsigned()->nullable();
            $table->integer('dictionary_id')->unsigned()->nullable();
            $table->string('meaning', 500);
            $table->string('audio_url', 500)->nullable();
            $table->string('meaning_image_url', 500)->nullable();
            $table->text('noun')->nullable();
            $table->text('pronoun')->nullable();
            $table->text('adjective')->nullable();
            $table->text('verb')->nullable();
            $table->text('adverb')->nullable();
            $table->text('singular')->nullable();
            $table->text('plural')->nullable();
            $table->text('synonym')->nullable();
            $table->text('antonym')->nullable();
            $table->text('origin')->nullable();
            $table->text('history')->nullable();
            $table->text('in_culture')->nullable();
            $table->text('in_medical')->nullable();
            $table->text('in_biology')->nullable();
            $table->text('in_engineering')->nullable();
            $table->text('in_science')->nullable();
            $table->text('confusing_with')->nullable();
            $table->text('related_with')->nullable();
            $table->text('related_with_phrases')->nullable();
            $table->string('image_url',500)->nullable();
            $table->boolean('is_published');
            $table->date('publish_date')->nullable();
            $table->string('publish_by', 100)->nullable();
            $table->timestamps();
            $table->foreign('dictionary_id')->references('id')->on('dictionaries');
            $table->foreign('word_temp_id')->references('id')->on('wordtemps');
           
        });
	
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('particulartemps');
    }
}
