<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWordtempsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wordtemps', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('dictionary_id')->unsigned()->nullable();
            $table->integer('group_id')->unsigned()->nullable();
            $table->string('dictionary_word', 200);
            $table->text('type');
            $table->string('audio_url')->nullable();
            $table->string('create_by', 50);
            $table->boolean('is_imported');
            $table->tinyInteger('status');
            $table->date('import_date')->nullable();
            $table->string('import_by')->nullable();
            $table->timestamps();
            $table->foreign('dictionary_id')->references('id')->on('dictionaries');
            $table->foreign('group_id')->references('id')->on('groups');

        });
		
	
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wordtemps');
    }
}
