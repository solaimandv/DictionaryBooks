<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',50);
            $table->string('email',56)->unique();
            $table->string('password',100);
            $table->string('phone',16);
            $table->integer('role_id')->unsigned()->nullable();
            $table->tinyInteger('status');
            $table->string('updated_by',50)->nullable();
            $table->string('blocked_by',50)->nullable();
            $table->date('blocked_date')->nullable();
            $table->text('blocked_reason')->nullable();
            $table->rememberToken();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            
            
        });
			 Schema::table('users', function($table) {
             $table->foreign('role_id')->references('id')->on('roles');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
