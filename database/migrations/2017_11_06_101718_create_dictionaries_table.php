<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDictionariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dictionaries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('dictionary_name', 16);
            $table->integer('language_from_id')->unsigned()->nullable();
            $table->integer('language_to_id')->unsigned()->nullable();
            $table->boolean('is_published');
            $table->timestamps();
            
        });
        	 Schema::table('dictionaries', function($table) {
             $table->foreign('language_from_id')->references('id')->on('languages');
             $table->foreign('language_to_id')->references('id')->on('languages');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dictionaries');
    }
}
