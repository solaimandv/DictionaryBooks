<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('words', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('word_temp_id')->unsigned()->nullable();
            $table->integer('dictionary_id')->unsigned()->nullable();
            $table->integer('group_id')->unsigned()->nullable();
            $table->string('dictionary_word', 200);
            $table->text('type');
            $table->string('pronouciation_url', 500)->nullable();
            $table->timestamps();
		});
			 Schema::table('words', function($table) {
             $table->foreign('word_temp_id')->references('id')->on('wordtemps');
             $table->foreign('dictionary_id')->references('id')->on('dictionaries');
             $table->foreign('group_id')->references('id')->on('groups');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('words');
    }
}
