<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'DictionaryController@index');


Route::get('/search-text', 'WordController@searchText');


//BackEnd Controller

Auth::routes();




Route::group(['middleware' => ['AuthenticateMiddleware']], function () {

Route::get('/dashboard', 'DashboardController@index'); 
// Word Temp
Route::get('/word-entry', 'WordtempController@create');
Route::post('/store/wordtemp', 'WordtempController@store');
Route::get('/edit-wordstemp/{id}', 'WordtempController@editword');
Route::post('/update-wordtemp', 'WordtempController@updatword');
Route::get('/delete-wordstemp/{id}', 'WordtempController@deleteword');
Route::get('/word/by-dictionary/{id}', 'WordtempController@getWordByDictionary');
// Particular Temp
Route::get('/particular-entry', 'ParticulartempController@create');
Route::post('/store/particular-temp', 'ParticulartempController@saveParticular');
Route::get('/view-particular', 'ParticulartempController@manage');
Route::get('/edit-particular/{id}', 'ParticulartempController@editParticular');
Route::post('/update-particular', 'ParticulartempController@updateParticular');
Route::get('/delete-particular/{id}', 'ParticulartempController@deleteParticular');
Route::get('/particular/review-word','ParticulartempController@reviewWord');

Route::get('/particular-extra','ParticulartempController@extraInfo');
Route::get('/particular-extrainfo/{dicId}/{status}/{id}','ParticulartempController@getExtraInfo');
Route::get('//particular-view/word/{dicId}/{status}/{id}','ParticulartempController@getParticularInfoById');

Route::post('/store-extraInfo','ParticulartempController@storeInfo');
Route::get('/search/word/{id}/{status}','ParticulartempController@getWord');



Route::group(['middleware'=>['OparatorCheck']],function(){

Route::get('/search-word', 'WordtempController@searchWord');

Route::get('/ajax/dicpublished', 'WordtempController@GetAllPublishedDictionary');
Route::get('/ajax/searchwordtemp/{dicId}/{typeId}/{searchText}', 'WordtempController@SearchWordTemp');


//  Particular Temp
Route::get('/ajax/showParticular/{id}', 'ParticulartempController@particularDetails');




// Particular 
Route::post('/ajax/saveParticular','ParticularController@particularSave');
Route::post('/review/particular','ParticularController@particularReview');






});

});















// test
Route::get('/view-test', 'ParticulartempController@test');
