<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Particular extends Model
{
    protected $fillable=['word_id','dictionary_id','meaning','meaning_image_url','noun','pronoun','adjective','verb','adverb','singular','plural','synonym','antonym','origin','history','in_culture','in_medical','in_biology','in_engineering','in_science','confusing_with','related_with','related_with_phrases','image_url','is_published','publish_date','publish_by'];
}
