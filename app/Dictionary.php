<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dictionary extends Model
{
    protected $fillable=['dictionaryName','language_from_id','language_to_id','is_published'];

    public function wordtemp()
    {
        return $this->hasMany('Wordtemp');
    }
    // public function friends()
    // {
    //     return $this->hasMany('Friend');
    // }
}
