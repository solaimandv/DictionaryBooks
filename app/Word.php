<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Word extends Model
{
    protected $fillabel=['dictionary_id','group_id','dictionary_word','type','pronouciation_url'];
}
