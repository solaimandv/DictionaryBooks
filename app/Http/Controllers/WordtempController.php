<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Wordtemp;
use App\Dictionary;
use App\Particulartemp;
use App\Group;
use Auth;
use Validator;
use DB;
 
 
class WordtempController extends Controller
{
 
    public function create()
    {
//   return Auth::check();
//  return Auth::user()->role_id;


        $dictionarys=Dictionary::all();
        $groups=Group::all();
        $wordtemps=DB::table('wordtemps')
        ->join('dictionaries', 'wordtemps.dictionary_id', '=', 'dictionaries.id')
        ->select('wordtemps.*', 'dictionaries.dictionary_name')
        ->orderBy('id', 'desc')
        // ->limit(10)
        ->paginate(10);       
        return view('admin.word.wordtempentry', compact('dictionarys', 'wordtemps','groups'));
    }
    public function store(Request $request)
    {    
        $wordBy = Wordtemp::where('dictionary_word', $request->dictionary_word)->first();
               
       if($wordBy==NULL)
       {
        $wordtemp=new Wordtemp();
        $wordtemp->dictionary_id=$request->dictionary_id;
        $wordtemp->dictionary_word=$request->dictionary_word;
        $wordtemp->group_id=$request->group_id;
        $wordtemp->type=$request->type;
        $wordtemp->create_by=$request->create_by;
        $wordtemp->status=$request->status;
        $wordtemp->is_imported=$request->is_imported;
        $wordtemp->import_date=$request->import_date;
        $wordtemp->import_by=$request->import_by; 
        $wordtemp->save();
        return redirect('/word-entry')->with('message', ' Word Saved Successfully !');
       }else{ 
        return redirect('/word-entry')->with('message', ' This Word Already Exist');
        }
       

       
    }

    public function editword($id)
    {
        $dictionarys = Dictionary::get();
        $groups=Group::all();
        $objWord = Wordtemp::where('id', $id)->first();
        $wordtemps = DB::table('wordtemps')
        ->join('dictionaries', 'wordtemps.dictionary_id', '=', 'dictionaries.id')
        ->select('wordtemps.*', 'dictionaries.dictionary_name')
        ->orderBy('id', 'desc')
        ->paginate(5);
        return view('admin.word.editword', compact('objWord', 'dictionarys', 'wordtemps','groups'));
    }

    public function updatword(Request $request)
    {       
        $wordtemp= Wordtemp::find($request->id);
        $wordtemp->dictionary_id=$request->dictionary_id;
        $wordtemp->dictionary_word=$request->dictionary_word;
        $wordtemp->group_id=$request->group_id;
        $wordtemp->type=$request->type;
        $wordtemp->create_by=$request->create_by;
        $wordtemp->status=$request->status;
        $wordtemp->import_date=$request->import_date;
        $wordtemp->is_imported=$request->is_imported;
        $wordtemp->import_by=$request->import_by;
        $wordtemp->save();
        return redirect('/word-entry')->with('message', ' Word Updated Successfully !');
    }
    public function deleteword($id)
    {
        // return $id;
        $particularTemp=Particulartemp::where('word_temp_id',$id)->first();      
        if($particularTemp)
        {
            return redirect('/word-entry')->with('message', 'Particular Word Already Exists.'); 
        }else{
            $wordtemp= Wordtemp::find($id);       
            $wordtemp->delete();
            return redirect('/word-entry')->with('message', ' Word Deleted Successfully !');
        }       
    }
    public function searchWord()
    {
        $dictionarys = Dictionary::get();
        $objWord = Wordtemp::limit(10)->get();
        // return $objWord;
        return view('admin.word.searchword', compact('objWord', 'dictionarys'));
    }

    public function GetAllPublishedDictionary()
    {
        $dictionarys = Dictionary::get();
        return json_encode($dictionarys);
    }

    public function SearchWordTemp($dicId,$typeId, $searchText)
    {  
        $searchs=DB::table('wordtemps')
        ->where('dictionary_ID', '=', $dicId)
        ->where('status', '=', $typeId)
        ->where('dictionary_word', 'like', '%'. $searchText .'%')
        ->get();
        return json_encode($searchs);
    }


    public function getWordByDictionary($id)
    {           
        $wordByDictionarys = DB::table('wordtemps')
        ->join('dictionaries', 'wordtemps.dictionary_id', '=', 'dictionaries.id')
        ->where('dictionary_id',$id)
        ->select('wordtemps.*', 'dictionaries.dictionary_name')
        ->orderBy('id', 'desc')
        ->limit(10)
       ->get();
        return json_encode($wordByDictionarys);
    }

}
