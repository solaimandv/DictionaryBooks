<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class WordController extends Controller
{
  public function searchText(Request $request)
  {
    if ($request->ajax())
    {        
        $words = DB::table('particulars')
            ->join('words','particulars.word_id','=','words.id')
            ->Where('words.dictionary_id','=', $request->dictionary_id)
            ->where('dictionary_word', '=', $request->search)         
            ->get();
    }
    return Response($words);
  }
}
