<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Particular;
use DB;
use App\Word;
use Auth;
class ParticularController extends Controller
{
   public function particularSave(Request $request)
   {
    $wordtemps=DB::table('wordtemps')->where('id','=',$request->word_temp_id)
    ->first();    
    $words=new Word();
    $words->word_temp_id=$wordtemps->id;
    $words->dictionary_id=$wordtemps->dictionary_id;
    $words->dictionary_word=$wordtemps->dictionary_word;
    $words->type=$wordtemps->type;
    $words->pronouciation_url=$wordtemps->audio_url;
    // $words->created_at=date('y-m-d h:i:s');
    $words->save();

// return json_encode($words->id);
//Insert into word table - get wordid/id from word table -set wordid/id to word_id in particulars
//object 
    $data = array();
    $data['particulartemp_id'] = $request->id;
    $data['word_id'] = $words->id;
    $data['dictionary_id'] = $request->dictionary_id;
    $data['meaning'] = $request->meaning;
    $data['audio_url'] = $request->audio_url;
    $data['meaning_image_url'] = $request->meaning_image_url;
    $data['noun'] = $request->noun;
    $data['pronoun'] = $request->pronoun;
    $data['adjective'] = $request->adjective;
    $data['verb'] = $request->verb;
    $data['adverb'] = $request->adverb;
    $data['singular'] = $request->singular;
    $data['plural'] = $request->plural;
    $data['synonym'] = $request->synonym;

    $data['in_science'] = $request->in_science;
    $data['in_medical'] = $request->in_medical;
    $data['in_biology'] = $request->in_biology;
    $data['in_engineering'] = $request->in_engineering;
    $data['in_culture'] = $request->in_culture;
    $data['history'] = $request->history;
    $data['image_url'] = $request->image_url;
    $data['confusing_with'] = $request->confusing_with;
    $data['related_with'] = $request->related_with;
    $data['related_with_phrases'] = $request->related_with_phrases; 


    $data['origin'] = $request->origin;
    $data['is_published'] = $request->is_published;
    $data['publish_date'] =date('Y-m-d');
    $data['publish_by'] =  Auth::user()->name;
    // return json_encode($data);
    DB::table('particulars')
    ->insert($data);

    $updated=DB::table('wordtemps')->where('id','=',$request->word_temp_id)
    ->update(array(
        'is_imported'=>2,
        'status'=>2
    ) );
    $updated=DB::table('particulartemps')->where('word_temp_id','=',$request->word_temp_id)
    ->update(array(
        'is_published'=>2,
        'publish_date'=>date('Y-m-d'),
        'publish_by'=>Auth::user()->name,
     ) );        
   }

public function particularReview(Request $request)
{  
 
$updated=DB::table('wordtemps')->where('id',$request->word_temp_id)
->update(array(
    'is_imported'=>1,          //1: Not Published, 2: Published  
    'status'=>3                  // 1: New, 2: Imported , 3: To Review ,  4: Reviewed 
) );


$updated=DB::table('particulartemps')->where('word_temp_id',$request->word_temp_id)
->update(array(
     'is_published'=>1,                    //  1: Not Published, 2:Published
    'publish_date'=>NULL,
    'publish_by'=>NULL
 ) );
 
}




}
