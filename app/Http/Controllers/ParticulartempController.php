<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Dictionary;
use App\Wordtemp;
use App\Particulartemp;
use DB;

class ParticulartempController extends Controller
{
   
    public function create()
    {
        $dictionarys=Dictionary::get();
        
        
        return view('admin.particular.particulartempentry', compact('dictionarys'));
    }


    public function saveParticular(Request $request)
    {
    //    return  $request->word_temp_id;
     
         if($request->status==3)
         {             
            $particulartemp=Particulartemp::find($request->id);            
            $particulartemp->dictionary_id=$request->dictionary_id;
            $particulartemp->word_temp_id=$request->word_temp_id;
            $particulartemp->meaning=$request->meaning;                     
            $particulartemp->noun=$request->noun;
            $particulartemp->pronoun=$request->pronoun;
            $particulartemp->adjective=$request->adjective;
            $particulartemp->verb=$request->verb;
            $particulartemp->adverb=$request->adverb;
            $particulartemp->singular=$request->singular;
            $particulartemp->plural=$request->plural;
            $particulartemp->synonym=$request->synonym;
            $particulartemp->antonym=$request->antonym;
            $particulartemp->origin=$request->origin;
            $particulartemp->is_published=$request->is_published;
            $particulartemp->publish_date=$request->publish_date;
            $particulartemp->publish_by=$request->publish_by;            
            // $particulartemp=$this->imageStatus($request); 
            $particularById=Particulartemp::where('id',$request->id)->first();
            $image=$request->file('meaning_image_url');  
            // return $image;          
            $audio=$request->file('audio_url');  
              
            if($image)
            {                
                $file =$particularById->meaning_image_url;
               if(file_exists($file))
               {
                    unlink($particularById->meaning_image_url);            
                    $ext = strtolower($image->getClientOriginalExtension());
                    $image_full_name = $particularById->id . '.' . $ext;                    
                    $upload_path = 'public/upload/images/';                    
                    $image_url = $upload_path.$image_full_name;                    
                    $success = $image->move($upload_path, $image_full_name);

               }else{
                    $ext = strtolower($image->getClientOriginalExtension());
                    $image_full_name = $particularById->id . '.' . $ext;                    
                    $upload_path = 'public/upload/images/';                    
                    $image_url = $upload_path.$image_full_name;                    
                    $success = $image->move($upload_path, $image_full_name);
               }                        
            }else{
              $image_url=$particularById->meaning_image_url;             
            }
            if($audio)
            {              
                $filename =$particularById->audio_url;                  
                if (file_exists($filename)) {
                    unlink($particularById->audio_url);                 
                    $ext = strtolower($audio->getClientOriginalExtension());
                    $audio_full_name = $particularById->id . '.' . $ext;
                    $upload_path = 'public/upload/files/';
                    $audio_url = $upload_path . $audio_full_name;
                    $success = $audio->move($upload_path, $audio_full_name);                    
                }else{
                    $ext = strtolower($audio->getClientOriginalExtension());
                    $audio_full_name = $particularById->id . '.' . $ext;
                    $upload_path = 'public/upload/files/';
                    $audio_url = $upload_path . $audio_full_name;
                    $success = $audio->move($upload_path, $audio_full_name);  
                }                           
            }else{
              $audio_url=$particularById->audio_url;            
            } 

            $particulartemp->meaning_image_url=$image_url;
            $particulartemp->audio_url=$audio_url;      
            $particulartemp->save();

            $wordtemps=Wordtemp::where('id',$request->word_temp_id)->update(['status' => 4]);
            return redirect('/particular-entry')->with('message', ' Word Updated Successfully !');
        
        }
        $particulartemp=new Particulartemp();
        $particulartemp->dictionary_id=$request->dictionary_id;
        $particulartemp->word_temp_id=$request->word_temp_id;
        $particulartemp->meaning=$request->meaning;
        $particulartemp->meaning_image_url=null;
        $particulartemp->audio_url=null;
        $particulartemp->noun=$request->noun;
        $particulartemp->pronoun=$request->pronoun;
        $particulartemp->adjective=$request->adjective;
        $particulartemp->verb=$request->verb;
        $particulartemp->adverb=$request->adverb;
        $particulartemp->singular=$request->singular;
        $particulartemp->plural=$request->plural;
        $particulartemp->synonym=$request->synonym;
        $particulartemp->antonym=$request->antonym;
        $particulartemp->origin=$request->origin;
        $particulartemp->is_published=$request->is_published;
        $particulartemp->publish_date=$request->publish_date;
        $particulartemp->publish_by=$request->publish_by;
        $particulartemp->save();

        $image = $request->file('meaning_image_url');
        if ($image) {
             $ext = strtolower($image->getClientOriginalExtension());
             $image_full_name = $particulartemp->id . '.' . $ext;
             $upload_path = 'public/upload/images/';
             $image_url = $upload_path . $image_full_name;
             $success = $image->move($upload_path, $image_full_name);
        } else {
            $image_url=null;
        }
            $file = $request->file('audio_url');
        if ($file) {
             $ext = strtolower($file->getClientOriginalExtension());
             $image_full_name = $particulartemp->id. '.' . $ext;
             $upload_path = 'public/upload/files/';
             $audio_url = $upload_path . $image_full_name;
             $success = $file->move($upload_path, $image_full_name);
        } else {
            $audio_url=null;
        }
        $particulartemp->meaning_image_url=$image_url;
        $particulartemp->audio_url=$audio_url;
        $particulartemp->save();
        return redirect('/particular-entry')->with('message', ' Particular Insert Successfully !');
    }




    
    public function manage()
    {
         $particularTemp=DB::table('Particulartemps')
         ->join('dictionaries', 'Particulartemps.dictionary_id', '=', 'dictionaries.id')
         ->join('wordtemps', 'Particulartemps.word_temp_id', '=', 'wordtemps.id')
         
         ->select('Particulartemps.*', 'dictionaries.dictionary_name', 'wordtemps.dictionary_word')
         ->orderBy('id', 'desc')
         ->limit(10)         
         ->get();
        
        $particulartemp=Particulartemp::get();
        $dictionarys=Dictionary::get();
        return view('admin.particular.manage', compact('particularTemp','dictionarys'));
    }

    public function editParticular($id)
    {
        $dictionarys=Dictionary::get();
        $objWord=WordTemp::get();
        $objParticular=DB::table('Particulartemps')        
        ->where('id', $id)
        ->first();    
       return view('admin.particular.editparticular', compact('objParticular', 'dictionarys', 'objWord'));
    }


    public function updateParticular(Request $request)
    {       
        // return $request;
       
        $particulartemp=Particulartemp::find($request->id);
        
        $particulartemp->dictionary_id=$request->dictionary_id;
        $particulartemp->word_temp_id=$request->word_temp_id;
        $particulartemp->meaning=$request->meaning; 
        // return $particulartemp->meaning;  
        
        // return $particulartemp->adjective;      
        $particulartemp->noun=$request->noun;
        $particulartemp->pronoun=$request->pronoun;
        $particulartemp->adjective=$request->adjective;
        $particulartemp->verb=$request->verb;
        $particulartemp->adverb=$request->adverb;
        $particulartemp->singular=$request->singular;
        $particulartemp->plural=$request->plural;
        $particulartemp->synonym=$request->synonym;
        $particulartemp->antonym=$request->antonym;
        $particulartemp->origin=$request->origin;
        $particulartemp->is_published=$request->is_published;
        $particulartemp->publish_date=$request->publish_date;
        $particulartemp->publish_by=$request->publish_by;       
        $particularById=Particulartemp::where('id',$request->id)->first();
        $image=$request->file('meaning_image_url');
        if($image)
        {          
            unlink($particularById->meaning_image_url);
               $ext = strtolower($image->getClientOriginalExtension());
               $image_full_name = $particularById->id . '.' . $ext;
               $upload_path = 'public/upload/images/';
               $image_url = $upload_path . $image_full_name;
               $success = $image->move($upload_path, $image_full_name);
        }else{
          $image_url=$particularById->meaning_image_url;        
        }
      
     $particulartemp->save();
        return redirect('/view-particular')->with('message', ' Word Updated Successfully !');
    }
        
    public function deleteParticular($id)
    {        
        $particular= Particulartemp::find($id);
        $particular->delete();
        return redirect('/view-particular')->with('message', ' Particular Temp Deleted Successfully !');
    }


public function particularDetails($id)
{
  $particular=DB::table('Particulartemps')
  ->where('word_temp_id','=',$id)
  ->first();
  return json_encode($particular);

}

public function extraInfo()
{
    $dictionarys=Dictionary::get();  
    
    return view('admin.particular.particulartemextra', compact('dictionarys'));
}

public function storeInfo(Request $request)
{    
    // return $request;
  $particulartemp=Particulartemp::where('word_temp_id',$request->word_temp_id)->first();
 ///return $particulartemp;
    if($particulartemp==!NULL){
    $particulartemp->history=$request->history;
    $particulartemp->in_culture=$request->in_culture;
    $particulartemp->in_medical=$request->in_medical;
    $particulartemp->in_biology=$request->in_biology;
    $particulartemp->in_engineering=$request->in_engineering;
    $particulartemp->in_science=$request->in_science;
        $image = $request->file('image_url');    
        if($image)
        {
            $ext = strtolower($image->getClientOriginalExtension());
            $image_full_name = $particulartemp->id . '.' . $ext;
            $upload_path = 'public/upload/Extraimages/';
            $image_url = $upload_path . $image_full_name;
            $success = $image->move($upload_path, $image_full_name);     
    $particulartemp->image_url=$image_url;
        }   
        
    $particulartemp->save();
        
return redirect('/particular-extra')->with('message', ' Extra Info Saved Successfully !');
}else{

    return redirect('/particular-extra')->with('message', ' This word have no particulars entry. Please entry particulars first !');

}
// dd($particulartemp);
}



public function getExtraInfo($dicId,$status,$id)
{ 
$particularTemp=Particulartemp::where('word_temp_id',$id)->select('image_url','in_culture','in_medical','in_biology','in_science','in_engineering','history')->first();
return json_encode($particularTemp);
}

public function getWord($id,$status)
{
$wordtemps=Wordtemp::where('dictionary_id',$id)->where('status',$status)->get();
return $wordtemps;
// return $id;
}


public function reviewWord(Request $request)
{ 
$particularTemp=Particulartemp::where('word_temp_id',$request->id)->first();
return json_encode($particularTemp);
}

public function getParticularInfoById($dicId,$status,$id)
{
    $particularById=DB::table('Particulartemps')
    ->join('dictionaries', 'Particulartemps.dictionary_id', '=', 'dictionaries.id')
    ->join('wordtemps', 'Particulartemps.word_temp_id', '=', 'wordtemps.id') 
    ->Where('wordtemps.id',$id)   
    ->select('Particulartemps.*', 'dictionaries.dictionary_name', 'wordtemps.dictionary_word')
    ->first();
    return json_encode($particularById);

}
}
