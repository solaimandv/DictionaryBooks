<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wordtemp extends Model
{
    protected $fillable=['dictionary_id','group_id','dictionary_word','type','create_by','is_imported','import_date','import_by'];
    public $timestamps = false;
    

    public function dictionary()
    {
         return $this->belongsTo('Dictionary');
    }
}
