-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 03, 2018 at 09:10 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.0.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_dictionary`
--

-- --------------------------------------------------------

--
-- Table structure for table `dictionaries`
--

CREATE TABLE `dictionaries` (
  `id` int(10) UNSIGNED NOT NULL,
  `dictionary_name` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL,
  `language_from_id` int(10) UNSIGNED DEFAULT NULL,
  `language_to_id` int(10) UNSIGNED DEFAULT NULL,
  `is_published` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `dictionaries`
--

INSERT INTO `dictionaries` (`id`, `dictionary_name`, `language_from_id`, `language_to_id`, `is_published`, `created_at`, `updated_at`) VALUES
(1, 'E2B', 1, 2, 1, '2017-12-02 18:00:00', NULL),
(2, 'B2E', 2, 1, 1, '2017-12-02 18:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `group_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `group_name`) VALUES
(1, 'Flower'),
(2, 'Human Body');

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` int(10) UNSIGNED NOT NULL,
  `language_name` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `language_name`, `created_at`, `updated_at`) VALUES
(1, 'English', '2017-12-02 18:00:00', NULL),
(2, 'Bangla', '2017-12-02 18:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_100000_create_password_resets_table', 1),
(2, '2017_11_06_100932_create_languages_table', 1),
(3, '2017_11_06_101718_create_dictionaries_table', 1),
(6, '2017_11_30_102411_create_roles_table', 1),
(12, '2014_10_12_000000_create_users_table', 2),
(13, '2017_12_06_064855_create_groups_table', 3),
(24, '2017_11_10_095502_create_wordtemps_table', 4),
(25, '2017_11_14_034552_create_particulartemps_table', 5),
(26, '2017_11_06_102255_create_words_table', 6),
(27, '2017_11_06_102905_create_particulars_table', 7);

-- --------------------------------------------------------

--
-- Table structure for table `particulars`
--

CREATE TABLE `particulars` (
  `id` int(10) UNSIGNED NOT NULL,
  `particulartemp_id` int(10) UNSIGNED DEFAULT NULL,
  `word_id` int(10) UNSIGNED DEFAULT NULL,
  `dictionary_id` int(10) UNSIGNED DEFAULT NULL,
  `meaning` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `audio_url` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meaning_image_url` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `noun` text COLLATE utf8mb4_unicode_ci,
  `pronoun` text COLLATE utf8mb4_unicode_ci,
  `adjective` text COLLATE utf8mb4_unicode_ci,
  `verb` text COLLATE utf8mb4_unicode_ci,
  `adverb` text COLLATE utf8mb4_unicode_ci,
  `singular` text COLLATE utf8mb4_unicode_ci,
  `plural` text COLLATE utf8mb4_unicode_ci,
  `synonym` text COLLATE utf8mb4_unicode_ci,
  `antonym` text COLLATE utf8mb4_unicode_ci,
  `origin` text COLLATE utf8mb4_unicode_ci,
  `history` text COLLATE utf8mb4_unicode_ci,
  `in_culture` text COLLATE utf8mb4_unicode_ci,
  `in_medical` text COLLATE utf8mb4_unicode_ci,
  `in_biology` text COLLATE utf8mb4_unicode_ci,
  `in_engineering` text COLLATE utf8mb4_unicode_ci,
  `in_science` text COLLATE utf8mb4_unicode_ci,
  `confusing_with` text COLLATE utf8mb4_unicode_ci,
  `related_with` text COLLATE utf8mb4_unicode_ci,
  `related_with_phrases` text COLLATE utf8mb4_unicode_ci,
  `image_url` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_published` tinyint(1) NOT NULL,
  `publish_date` date DEFAULT NULL,
  `publish_by` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `particulars`
--

INSERT INTO `particulars` (`id`, `particulartemp_id`, `word_id`, `dictionary_id`, `meaning`, `audio_url`, `meaning_image_url`, `noun`, `pronoun`, `adjective`, `verb`, `adverb`, `singular`, `plural`, `synonym`, `antonym`, `origin`, `history`, `in_culture`, `in_medical`, `in_biology`, `in_engineering`, `in_science`, `confusing_with`, `related_with`, `related_with_phrases`, `image_url`, `is_published`, `publish_date`, `publish_by`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 'কাজ', 'public/upload/files/1.mp3', 'public/upload/images/1.png', 'কাজ, কর্ম, শ্রম, চাকরি, চাকরী, সেবা, অবস্থা, আইন, অনুশীলন, উদ্দেশ্য, কারণ, সাধনা, দলিল', 'কাজ, করা, পরিচালনা করা, ক্রিয়া, ক্রীতদাস, যাওয়া, চলা, চালান, পদব্রজে ভ্রমণ, অবিরত, শ্রম, পরিশ্রম, চেষ্টা', 'কাজ, করা, পরিচালনা করা, ক্রিয়া, ক্রীতদাস, যাওয়া, চলা, চালান, পদব্রজে ভ্রমণ, অবিরত, শ্রম, পরিশ্রম, চেষ্টা', NULL, NULL, NULL, NULL, '<p>1. work :: কাজ</p><p>2. job :: কাজ</p><p>3. action :: কর্ম</p><p>4. assignment :: কাজ</p><p>5. labor :: শ্রম</p>', NULL, 'English to Bengali Dictionary: work<br>Meaning and definitions of work, translation in Bengali language for work with similar and opposite words. Also find spoken pronunciation of work in Bengali and in English language.<br><br>Tags for the entry \"work\"<br>What work means in Bengali, work meaning in Bengali, work definition, examples and pronunciation of work in Bengali language.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2017-12-06', 'Solaiman Ahmed', NULL, NULL),
(2, 3, 2, 1, 'বাস্তব', 'public/upload/files/3.mp3', 'public/upload/images/3.jpg', 'বাস্তব, আসল, শারীরিক, সত্য, মূল, অকৃত্রিম, খাঁটি, বাস্তবিক, সারগর্ভ, কার্যকর, অধিকার, নির্ভুল, যথার্থ, আন্তরিক', NULL, 'বাস্তববাদ, বাস্তব, বাস্তবতা, প্রভাব, বাস্তবিকতা, ঐতিহাসিক সত্য, সত্য', NULL, NULL, NULL, NULL, '<p>1. real :: বাস্তব</p><p>2. tangible :: বাস্তব</p><p>3. substantive :: বাস্তব</p><p>4. actual :: আসল</p><p>5. physical :: শারীরিক</p><div><br></div>', NULL, '<p>English to Bengali Dictionary: real<br>Meaning and definitions of real, translation in Bengali language for real with similar and opposite words. Also find spoken pronunciation of real in Bengali and in English language.<br><br>Tags for the entry \"real\"<br>What real means in Bengali, real meaning in Bengali, real definition, examples and pronunciation of real in Bengali language.</p>', 'English to Bengali Dictionary: real<br>Meaning and definitions of real, translation in Bengali language for real with similar and opposite words. Also find spoken pronunciation of real in Bengali and in English language.<br><br>Tags for the entry \"real\"<br>What real means in Bengali, real meaning in Bengali, real definition, examples and pronunciation of real in Bengali language.', '<p>6. cereal bowl :: খাদ্যশস্য বাটি</p><p>7. cereal box :: খাদ্যশস্য বক্স</p><p>8. cereal crops :: খাদ্যশস্য ফসলের</p><p>9. cereal grain :: শস্য দানা</p>', '<p>1. arboreal salamander :: বৃক্ষবাসী স্যালামাণ্ডার</p><p>2. augmented reality :: উদ্দীপিত বাস্তবতা</p><p>3. be realistic :: বাস্তববাদী হও</p><p>4. boreal forest :: উত্তরের বনের</p><div><br></div>', '<p>1. arboreal salamander :: বৃক্ষবাসী স্যালামাণ্ডার</p><p>2. augmented reality :: উদ্দীপিত বাস্তবতা</p><p>3. be realistic :: বাস্তববাদী হও</p><p>4. boreal forest :: উত্তরের বনের</p><div><br></div>', '<p>1. arboreal salamander :: বৃক্ষবাসী স্যালামাণ্ডার</p><p>2. augmented reality :: উদ্দীপিত বাস্তবতা</p><p>3. be realistic :: বাস্তববাদী হও</p><p>4. boreal forest :: উত্তরের বনের</p><div><br></div>', '<p>1. arboreal salamander :: বৃক্ষবাসী স্যালামাণ্ডার</p><p>2. augmented reality :: উদ্দীপিত বাস্তবতা</p><p>3. be realistic :: বাস্তববাদী হও</p><p>4. boreal forest :: উত্তরের বনের</p><div><br></div>', NULL, NULL, NULL, 'public/upload/Extraimages/3.png', 1, '2017-12-07', 'Solaiman Ahmed', NULL, NULL),
(3, 4, 3, 1, 'Wash', 'public/upload/files/4.mp3', 'public/upload/images/4.jpg', 'Wash', 'Wash', 'Wash', 'Wash', NULL, 'Wash', NULL, NULL, NULL, NULL, 'Wash', 'Wash', 'Wash', NULL, 'Wash', 'Wash', NULL, NULL, NULL, 'public/upload/Extraimages/4.png', 1, '2017-12-10', 'Solaiman Ahmed', NULL, NULL),
(4, 4, 4, 1, 'Wash  Wash', 'public/upload/files/4.mp3', 'public/upload/images/4.jpg', 'Wash', 'Wash', 'Wash', 'Wash', NULL, 'Wash', NULL, NULL, NULL, 'wwwwwwwww', 'Wash Washing', 'Wash', 'Wash', NULL, 'Wash', 'Wash', NULL, NULL, NULL, 'public/upload/Extraimages/4.png', 1, '2017-12-21', 'Solaiman Ahmed', NULL, NULL),
(5, 1, 5, 1, 'কাজ', 'public/upload/files/1.mp3', 'public/upload/images/1.png', 'কাজ, কর্ম, শ্রম, চাকরি, চাকরী, সেবা, অবস্থা, আইন, অনুশীলন, উদ্দেশ্য, কারণ, সাধনা, দলিল', 'কাজ, করা, পরিচালনা করা, ক্রিয়া, ক্রীতদাস, যাওয়া, চলা, চালান, পদব্রজে ভ্রমণ, অবিরত, শ্রম, পরিশ্রম, চেষ্টা', 'কাজ, করা, পরিচালনা করা, ক্রিয়া, ক্রীতদাস, যাওয়া, চলা, চালান, পদব্রজে ভ্রমণ, অবিরত, শ্রম, পরিশ্রম, চেষ্টা', NULL, NULL, NULL, NULL, '<p>1. work :: কাজ</p><p>2. job :: কাজ</p><p>3. action :: কর্ম</p><p>4. assignment :: কাজ</p><p>5. labor :: শ্রম</p>', NULL, 'English to Bengali Dictionary: work<br>Meaning and definitions of work, translation in Bengali language for work with similar and opposite words. Also find spoken pronunciation of work in Bengali and in English language.<br><br>Tags for the entry \"work\"<br>What work means in Bengali, work meaning in Bengali, work definition, examples and pronunciation of work in Bengali language.', 'Work Something&nbsp;', 'Work Something&nbsp;', 'Work Something&nbsp;', 'Work Something Work Something Work Something&nbsp;', 'Work Something&nbsp;', 'Work Something&nbsp;', NULL, NULL, NULL, 'public/upload/Extraimages/1.jpg', 1, '2017-12-21', 'Solaiman Ahmed', NULL, NULL),
(6, 4, 6, 1, 'Wash  Wash', 'public/upload/files/4.mp3', 'public/upload/images/4.jpg', 'Wash', 'Wash', 'Wash', 'Wash', NULL, 'Wash', NULL, NULL, NULL, 'wwwwwwwww', 'Wash Washing', 'Wash', 'Wash', NULL, 'Wash', 'Wash', NULL, NULL, NULL, 'public/upload/Extraimages/4.png', 1, '2017-12-21', 'Solaiman Ahmed', NULL, NULL),
(7, 1, 7, 1, 'কাজ', 'public/upload/files/1.mp3', 'public/upload/images/1.png', 'কাজ, কর্ম, শ্রম, চাকরি, চাকরী, সেবা, অবস্থা, আইন, অনুশীলন, উদ্দেশ্য, কারণ, সাধনা, দলিল', 'কাজ, করা, পরিচালনা করা, ক্রিয়া, ক্রীতদাস, যাওয়া, চলা, চালান, পদব্রজে ভ্রমণ, অবিরত, শ্রম, পরিশ্রম, চেষ্টা', 'কাজ, করা, পরিচালনা করা, ক্রিয়া, ক্রীতদাস, যাওয়া, চলা, চালান, পদব্রজে ভ্রমণ, অবিরত, শ্রম, পরিশ্রম, চেষ্টা', NULL, NULL, NULL, NULL, '<p>1. work :: কাজ</p><p>2. job :: কাজ</p><p>3. action :: কর্ম</p><p>4. assignment :: কাজ</p><p>5. labor :: শ্রম</p>', NULL, 'English to Bengali Dictionary: work<br>Meaning and definitions of work, translation in Bengali language for work with similar and opposite words. Also find spoken pronunciation of work in Bengali and in English language.<br><br>Tags for the entry \"work\"<br>What work means in Bengali, work meaning in Bengali, work definition, examples and pronunciation of work in Bengali language.', 'Work Something&nbsp;', 'Work Something&nbsp;', 'Work Something&nbsp;', 'Work Something Work Something Work Something&nbsp;', 'Work Something&nbsp;', 'Work Something&nbsp;', NULL, NULL, NULL, 'public/upload/Extraimages/1.jpg', 1, '2017-12-21', 'Solaiman Ahmed', NULL, NULL),
(8, 1, 8, 1, 'কাজ', 'public/upload/files/1.mp3', 'public/upload/images/1.png', 'কাজ, কর্ম, শ্রম, চাকরি, চাকরী, সেবা, অবস্থা, আইন, অনুশীলন, উদ্দেশ্য, কারণ, সাধনা, দলিল', 'কাজ, করা, পরিচালনা করা, ক্রিয়া, ক্রীতদাস, যাওয়া, চলা, চালান, পদব্রজে ভ্রমণ, অবিরত, শ্রম, পরিশ্রম, চেষ্টা', 'কাজ, করা, পরিচালনা করা, ক্রিয়া, ক্রীতদাস, যাওয়া, চলা, চালান, পদব্রজে ভ্রমণ, অবিরত, শ্রম, পরিশ্রম, চেষ্টা', NULL, NULL, NULL, NULL, '<p>1. work :: কাজ</p><p>2. job :: কাজ</p><p>3. action :: কর্ম</p><p>4. assignment :: কাজ</p><p>5. labor :: শ্রম</p>', NULL, 'English to Bengali Dictionary: work<br>Meaning and definitions of work, translation in Bengali language for work with similar and opposite words. Also find spoken pronunciation of work in Bengali and in English language.<br><br>Tags for the entry \"work\"<br>What work means in Bengali, work meaning in Bengali, work definition, examples and pronunciation of work in Bengali language.', 'Work Something&nbsp;', 'Work Something&nbsp;', 'Work Something&nbsp;', 'Work Something Work Something Work Something&nbsp;', 'Work Something&nbsp;', 'Work Something&nbsp;', NULL, NULL, NULL, 'public/upload/Extraimages/1.jpg', 1, '2017-12-21', 'Solaiman Ahmed', NULL, NULL),
(9, 1, 9, 1, 'কাজ', 'public/upload/files/1.mp3', 'public/upload/images/1.png', 'কাজ, কর্ম, শ্রম, চাকরি, চাকরী, সেবা, অবস্থা, আইন, অনুশীলন, উদ্দেশ্য, কারণ, সাধনা, দলিল', 'কাজ, করা, পরিচালনা করা, ক্রিয়া, ক্রীতদাস, যাওয়া, চলা, চালান, পদব্রজে ভ্রমণ, অবিরত, শ্রম, পরিশ্রম, চেষ্টা', 'কাজ, করা, পরিচালনা করা, ক্রিয়া, ক্রীতদাস, যাওয়া, চলা, চালান, পদব্রজে ভ্রমণ, অবিরত, শ্রম, পরিশ্রম, চেষ্টা', NULL, NULL, NULL, NULL, '<p>1. work :: কাজ</p><p>2. job :: কাজ</p><p>3. action :: কর্ম</p><p>4. assignment :: কাজ</p><p>5. labor :: শ্রম</p>', NULL, 'English to Bengali Dictionary: work<br>Meaning and definitions of work, translation in Bengali language for work with similar and opposite words. Also find spoken pronunciation of work in Bengali and in English language.<br><br>Tags for the entry \"work\"<br>What work means in Bengali, work meaning in Bengali, work definition, examples and pronunciation of work in Bengali language.', 'Work Something&nbsp;', 'Work Something&nbsp;', 'Work Something&nbsp;', 'Work Something Work Something Work Something&nbsp;', 'Work Something&nbsp;', 'Work Something&nbsp;', NULL, NULL, NULL, 'public/upload/Extraimages/1.jpg', 1, '2017-12-21', 'Solaiman Ahmed', NULL, NULL),
(10, 1, 10, 1, 'কাজ', 'public/upload/files/1.mp3', 'public/upload/images/1.png', 'কাজ, কর্ম, শ্রম, চাকরি, চাকরী, সেবা, অবস্থা, আইন, অনুশীলন, উদ্দেশ্য, কারণ, সাধনা, দলিল', 'কাজ, করা, পরিচালনা করা, ক্রিয়া, ক্রীতদাস, যাওয়া, চলা, চালান, পদব্রজে ভ্রমণ, অবিরত, শ্রম, পরিশ্রম, চেষ্টা', 'কাজ, করা, পরিচালনা করা, ক্রিয়া, ক্রীতদাস, যাওয়া, চলা, চালান, পদব্রজে ভ্রমণ, অবিরত, শ্রম, পরিশ্রম, চেষ্টা', NULL, NULL, NULL, NULL, '<p>1. work :: কাজ</p><p>2. job :: কাজ</p><p>3. action :: কর্ম</p><p>4. assignment :: কাজ</p><p>5. labor :: শ্রম</p>', NULL, 'English to Bengali Dictionary: work<br>Meaning and definitions of work, translation in Bengali language for work with similar and opposite words. Also find spoken pronunciation of work in Bengali and in English language.<br><br>Tags for the entry \"work\"<br>What work means in Bengali, work meaning in Bengali, work definition, examples and pronunciation of work in Bengali language.', 'Work Something&nbsp;', 'Work Something&nbsp;', 'Work Something&nbsp;', 'Work Something Work Something Work Something&nbsp;', 'Work Something&nbsp;', 'Work Something&nbsp;', NULL, NULL, NULL, 'public/upload/Extraimages/1.jpg', 1, '2017-12-21', 'Solaiman Ahmed', NULL, NULL),
(11, 1, 11, 1, 'কাজ', 'public/upload/files/1.mp3', 'public/upload/images/1.png', 'কাজ, কর্ম, শ্রম, চাকরি, চাকরী, সেবা, অবস্থা, আইন, অনুশীলন, উদ্দেশ্য, কারণ, সাধনা, দলিল', 'কাজ, করা, পরিচালনা করা, ক্রিয়া, ক্রীতদাস, যাওয়া, চলা, চালান, পদব্রজে ভ্রমণ, অবিরত, শ্রম, পরিশ্রম, চেষ্টা', 'কাজ, করা, পরিচালনা করা, ক্রিয়া, ক্রীতদাস, যাওয়া, চলা, চালান, পদব্রজে ভ্রমণ, অবিরত, শ্রম, পরিশ্রম, চেষ্টা', NULL, NULL, NULL, NULL, '<p>1. work :: কাজ</p><p>2. job :: কাজ</p><p>3. action :: কর্ম</p><p>4. assignment :: কাজ</p><p>5. labor :: শ্রম</p>', NULL, 'English to Bengali Dictionary: work<br>Meaning and definitions of work, translation in Bengali language for work with similar and opposite words. Also find spoken pronunciation of work in Bengali and in English language.<br><br>Tags for the entry \"work\"<br>What work means in Bengali, work meaning in Bengali, work definition, examples and pronunciation of work in Bengali language.', 'Work Something&nbsp;', 'Work Something&nbsp;', 'Work Something&nbsp;', 'Work Something Work Something Work Something&nbsp;', 'Work Something&nbsp;', 'Work Something&nbsp;', NULL, NULL, NULL, 'public/upload/Extraimages/1.jpg', 1, '2017-12-21', 'Solaiman Ahmed', NULL, NULL),
(12, 1, 12, 1, 'কাজ', 'public/upload/files/1.mp3', 'public/upload/images/1.png', 'কাজ, কর্ম, শ্রম, চাকরি, চাকরী, সেবা, অবস্থা, আইন, অনুশীলন, উদ্দেশ্য, কারণ, সাধনা, দলিল', 'কাজ, করা, পরিচালনা করা, ক্রিয়া, ক্রীতদাস, যাওয়া, চলা, চালান, পদব্রজে ভ্রমণ, অবিরত, শ্রম, পরিশ্রম, চেষ্টা', 'কাজ, করা, পরিচালনা করা, ক্রিয়া, ক্রীতদাস, যাওয়া, চলা, চালান, পদব্রজে ভ্রমণ, অবিরত, শ্রম, পরিশ্রম, চেষ্টা', NULL, NULL, NULL, NULL, '<p>1. work :: কাজ</p><p>2. job :: কাজ</p><p>3. action :: কর্ম</p><p>4. assignment :: কাজ</p><p>5. labor :: শ্রম</p>', NULL, 'English to Bengali Dictionary: work<br>Meaning and definitions of work, translation in Bengali language for work with similar and opposite words. Also find spoken pronunciation of work in Bengali and in English language.<br><br>Tags for the entry \"work\"<br>What work means in Bengali, work meaning in Bengali, work definition, examples and pronunciation of work in Bengali language.', 'Work Something&nbsp;', 'Work Something&nbsp;', 'Work Something&nbsp;', 'Work Something Work Something Work Something&nbsp;', 'Work Something&nbsp;', 'Work Something&nbsp;', NULL, NULL, NULL, 'public/upload/Extraimages/1.jpg', 1, '2017-12-21', 'Solaiman Ahmed', NULL, NULL),
(13, 4, 13, 1, 'Wash  Wash', 'public/upload/files/4.mp3', 'public/upload/images/4.jpg', 'Wash', 'Wash', 'Wash', 'Wash', NULL, 'Wash', NULL, NULL, NULL, 'wwwwwwwww', 'Wash Washing', 'Wash', 'Wash', NULL, 'Wash', 'Wash', NULL, NULL, NULL, 'public/upload/Extraimages/4.png', 1, '2017-12-21', 'Solaiman Ahmed', NULL, NULL),
(14, 1, 14, 1, 'কাজ', 'public/upload/files/1.mp3', 'public/upload/images/1.png', 'কাজ, কর্ম, শ্রম, চাকরি, চাকরী, সেবা, অবস্থা, আইন, অনুশীলন, উদ্দেশ্য, কারণ, সাধনা, দলিল', 'কাজ, করা, পরিচালনা করা, ক্রিয়া, ক্রীতদাস, যাওয়া, চলা, চালান, পদব্রজে ভ্রমণ, অবিরত, শ্রম, পরিশ্রম, চেষ্টা', 'কাজ, করা, পরিচালনা করা, ক্রিয়া, ক্রীতদাস, যাওয়া, চলা, চালান, পদব্রজে ভ্রমণ, অবিরত, শ্রম, পরিশ্রম, চেষ্টা', NULL, NULL, NULL, NULL, '<p>1. work :: কাজ</p><p>2. job :: কাজ</p><p>3. action :: কর্ম</p><p>4. assignment :: কাজ</p><p>5. labor :: শ্রম</p>', NULL, 'English to Bengali Dictionary: work<br>Meaning and definitions of work, translation in Bengali language for work with similar and opposite words. Also find spoken pronunciation of work in Bengali and in English language.<br><br>Tags for the entry \"work\"<br>What work means in Bengali, work meaning in Bengali, work definition, examples and pronunciation of work in Bengali language.', 'Work Something&nbsp;', 'Work Something&nbsp;', 'Work Something&nbsp;', 'Work Something Work Something Work Something&nbsp;', 'Work Something&nbsp;', 'Work Something&nbsp;', NULL, NULL, NULL, 'public/upload/Extraimages/1.jpg', 1, '2017-12-21', 'Solaiman Ahmed', NULL, NULL),
(15, 1, 15, 1, 'কাজ', 'public/upload/files/1.mp3', 'public/upload/images/1.png', 'কাজ, কর্ম, শ্রম, চাকরি, চাকরী, সেবা, অবস্থা, আইন, অনুশীলন, উদ্দেশ্য, কারণ, সাধনা, দলিল', 'কাজ, করা, পরিচালনা করা, ক্রিয়া, ক্রীতদাস, যাওয়া, চলা, চালান, পদব্রজে ভ্রমণ, অবিরত, শ্রম, পরিশ্রম, চেষ্টা', 'কাজ, করা, পরিচালনা করা, ক্রিয়া, ক্রীতদাস, যাওয়া, চলা, চালান, পদব্রজে ভ্রমণ, অবিরত, শ্রম, পরিশ্রম, চেষ্টা', NULL, NULL, NULL, NULL, '<p>1. work :: কাজ</p><p>2. job :: কাজ</p><p>3. action :: কর্ম</p><p>4. assignment :: কাজ</p><p>5. labor :: শ্রম</p>', NULL, 'English to Bengali Dictionary: work<br>Meaning and definitions of work, translation in Bengali language for work with similar and opposite words. Also find spoken pronunciation of work in Bengali and in English language.<br><br>Tags for the entry \"work\"<br>What work means in Bengali, work meaning in Bengali, work definition, examples and pronunciation of work in Bengali language.', 'Work Something&nbsp;', 'Work Something&nbsp;', 'Work Something&nbsp;', 'Work Something Work Something Work Something&nbsp;', 'Work Something&nbsp;', 'Work Something&nbsp;', NULL, NULL, NULL, 'public/upload/Extraimages/1.jpg', 1, '2017-12-21', 'Solaiman Ahmed', NULL, NULL),
(16, 1, 16, 1, 'কাজ', 'public/upload/files/1.mp3', 'public/upload/images/1.png', 'কাজ, কর্ম, শ্রম, চাকরি, চাকরী, সেবা, অবস্থা, আইন, অনুশীলন, উদ্দেশ্য, কারণ, সাধনা, দলিল', 'কাজ, করা, পরিচালনা করা, ক্রিয়া, ক্রীতদাস, যাওয়া, চলা, চালান, পদব্রজে ভ্রমণ, অবিরত, শ্রম, পরিশ্রম, চেষ্টা', 'কাজ, করা, পরিচালনা করা, ক্রিয়া, ক্রীতদাস, যাওয়া, চলা, চালান, পদব্রজে ভ্রমণ, অবিরত, শ্রম, পরিশ্রম, চেষ্টা', NULL, NULL, NULL, NULL, '<p>1. work :: কাজ</p><p>2. job :: কাজ</p><p>3. action :: কর্ম</p><p>4. assignment :: কাজ</p><p>5. labor :: শ্রম</p>', NULL, 'English to Bengali Dictionary: work<br>Meaning and definitions of work, translation in Bengali language for work with similar and opposite words. Also find spoken pronunciation of work in Bengali and in English language.<br><br>Tags for the entry \"work\"<br>What work means in Bengali, work meaning in Bengali, work definition, examples and pronunciation of work in Bengali language.', 'Work Something&nbsp;', 'Work Something&nbsp;', 'Work Something&nbsp;', 'Work Something Work Something Work Something&nbsp;', 'Work Something&nbsp;', 'Work Something&nbsp;', NULL, NULL, NULL, 'public/upload/Extraimages/1.jpg', 1, '2017-12-21', 'Solaiman Ahmed', NULL, NULL),
(17, 4, 17, 1, 'Wash  Wash', 'public/upload/files/4.mp3', 'public/upload/images/4.jpg', 'Wash', 'Wash', 'Wash', 'Wash', NULL, 'Wash', NULL, NULL, NULL, 'wwwwwwwww', 'Wash Washing', 'Wash', 'Wash', NULL, 'Wash', 'Wash', NULL, NULL, NULL, 'public/upload/Extraimages/4.png', 1, '2017-12-21', 'Solaiman Ahmed', NULL, NULL),
(18, 1, 18, 1, 'কাজ', 'public/upload/files/1.mp3', 'public/upload/images/1.png', 'কাজ, কর্ম, শ্রম, চাকরি, চাকরী, সেবা, অবস্থা, আইন, অনুশীলন, উদ্দেশ্য, কারণ, সাধনা, দলিল', 'কাজ, করা, পরিচালনা করা, ক্রিয়া, ক্রীতদাস, যাওয়া, চলা, চালান, পদব্রজে ভ্রমণ, অবিরত, শ্রম, পরিশ্রম, চেষ্টা', 'কাজ, করা, পরিচালনা করা, ক্রিয়া, ক্রীতদাস, যাওয়া, চলা, চালান, পদব্রজে ভ্রমণ, অবিরত, শ্রম, পরিশ্রম, চেষ্টা', NULL, NULL, NULL, NULL, '<p>1. work :: কাজ</p><p>2. job :: কাজ</p><p>3. action :: কর্ম</p><p>4. assignment :: কাজ</p><p>5. labor :: শ্রম</p>', NULL, 'English to Bengali Dictionary: work<br>Meaning and definitions of work, translation in Bengali language for work with similar and opposite words. Also find spoken pronunciation of work in Bengali and in English language.<br><br>Tags for the entry \"work\"<br>What work means in Bengali, work meaning in Bengali, work definition, examples and pronunciation of work in Bengali language.', 'Work Something&nbsp;', 'Work Something&nbsp;', 'Work Something&nbsp;', 'Work Something Work Something Work Something&nbsp;', 'Work Something&nbsp;', 'Work Something&nbsp;', NULL, NULL, NULL, 'public/upload/Extraimages/1.jpg', 1, '2017-12-21', 'Solaiman Ahmed', NULL, NULL),
(19, 5, 19, 1, 'হৃদয়', NULL, NULL, 'হৃদয়, বক্ষ, স্তন, মন, মনোযোগ, আত্মা, চিন্তা, স্মৃতি, অন্তর, শেষ, ভিতরে, গভীরতা, আন্তরিকতা, সাহস', 'হৃদয়', NULL, NULL, NULL, NULL, NULL, '<p>1. heart :: হৃদয়</p><p>2. bosom :: বক্ষ</p><p>3. breast :: স্তন</p><p>4. mind :: মন</p>', NULL, 'English to Bengali Dictionary: heart<br>Meaning and definitions of heart, translation in Bengali language for heart with similar and opposite words. Also find spoken pronunciation of heart in Bengali and in English language.<br><br>Tags for the entry \"heart\"<br>What heart means in Bengali, heart meaning in Bengali, heart definition, examples and pronunciation of heart in Bengali language.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2017-12-21', 'Solaiman Ahmed', NULL, NULL),
(20, 3, 20, 1, 'বাস্তব', 'public/upload/files/3.mp3', 'public/upload/images/3.png', 'বাস্তব, আসল, শারীরিক, সত্য, মূল, অকৃত্রিম, খাঁটি, বাস্তবিক, সারগর্ভ, কার্যকর, অধিকার, নির্ভুল, যথার্থ, আন্তরিক', NULL, 'বাস্তববাদ, বাস্তব, বাস্তবতা, প্রভাব, বাস্তবিকতা, ঐতিহাসিক সত্য, সত্য', NULL, NULL, NULL, NULL, '<p>1. real :: বাস্তব</p><p>2. tangible :: বাস্তব</p><p>3. substantive :: বাস্তব</p><p>4. actual :: আসল</p><p>5. physical :: শারীরিক</p><div><br></div>', NULL, '<p>English to Bengali Dictionary: real<br>Meaning and definitions of real, translation in Bengali language for real with similar and opposite words. Also find spoken pronunciation of real in Bengali and in English language.<br><br>Tags for the entry \"real\"<br>What real means in Bengali, real meaning in Bengali, real definition, examples and pronunciation of real in Bengali language.</p>', 'English to Bengali Dictionary: real<br>Meaning and definitions of real, translation in Bengali language for real with similar and opposite words. Also find spoken pronunciation of real in Bengali and in English language.<br><br>Tags for the entry \"real\"<br>What real means in Bengali, real meaning in Bengali, real definition, examples and pronunciation of real in Bengali language.', '<p>6. cereal bowl :: খাদ্যশস্য বাটি</p><p>7. cereal box :: খাদ্যশস্য বক্স</p><p>8. cereal crops :: খাদ্যশস্য ফসলের</p><p>9. cereal grain :: শস্য দানা</p>', '<p>1. arboreal salamander :: বৃক্ষবাসী স্যালামাণ্ডার</p><p>2. augmented reality :: উদ্দীপিত বাস্তবতা</p><p>3. be realistic :: বাস্তববাদী হও</p><p>4. boreal forest :: উত্তরের বনের</p><div><br></div>', '<p>1. arboreal salamander :: বৃক্ষবাসী স্যালামাণ্ডার</p><p>2. augmented reality :: উদ্দীপিত বাস্তবতা</p><p>3. be realistic :: বাস্তববাদী হও</p><p>4. boreal forest :: উত্তরের বনের</p><div><br></div>', '<p>1. arboreal salamander :: বৃক্ষবাসী স্যালামাণ্ডার</p><p>2. augmented reality :: উদ্দীপিত বাস্তবতা</p><p>3. be realistic :: বাস্তববাদী হও</p><p>4. boreal forest :: উত্তরের বনের</p><div><br></div>', '<p>1. arboreal salamander :: বৃক্ষবাসী স্যালামাণ্ডার</p><p>2. augmented reality :: উদ্দীপিত বাস্তবতা</p><p>3. be realistic :: বাস্তববাদী হও</p><p>4. boreal forest :: উত্তরের বনের</p><div><br></div>', NULL, NULL, NULL, 'public/upload/Extraimages/3.png', 1, '2017-12-21', 'Solaiman Ahmed', NULL, NULL),
(21, 1, 21, 1, 'কাজ', 'public/upload/files/1.mp3', 'public/upload/images/1.png', 'কাজ, কর্ম, শ্রম, চাকরি, চাকরী, সেবা, অবস্থা, আইন, অনুশীলন, উদ্দেশ্য, কারণ, সাধনা, দলিল', 'কাজ, করা, পরিচালনা করা, ক্রিয়া, ক্রীতদাস, যাওয়া, চলা, চালান, পদব্রজে ভ্রমণ, অবিরত, শ্রম, পরিশ্রম, চেষ্টা', 'কাজ, করা, পরিচালনা করা, ক্রিয়া, ক্রীতদাস, যাওয়া, চলা, চালান, পদব্রজে ভ্রমণ, অবিরত, শ্রম, পরিশ্রম, চেষ্টা', NULL, NULL, NULL, NULL, '<p>1. work :: কাজ</p><p>2. job :: কাজ</p><p>3. action :: কর্ম</p><p>4. assignment :: কাজ</p><p>5. labor :: শ্রম</p>', NULL, 'English to Bengali Dictionary: work<br>Meaning and definitions of work, translation in Bengali language for work with similar and opposite words. Also find spoken pronunciation of work in Bengali and in English language.<br><br>Tags for the entry \"work\"<br>What work means in Bengali, work meaning in Bengali, work definition, examples and pronunciation of work in Bengali language.', 'Work Something&nbsp;', 'Work Something&nbsp;', 'Work Something&nbsp;', 'Work Something Work Something Work Something&nbsp;', 'Work Something&nbsp;', 'Work Something&nbsp;', NULL, NULL, NULL, 'public/upload/Extraimages/1.jpg', 1, '2017-12-21', 'Solaiman Ahmed', NULL, NULL),
(22, 1, 22, 1, 'কাজ', 'public/upload/files/1.mp3', 'public/upload/images/1.png', 'কাজ, কর্ম, শ্রম, চাকরি, চাকরী, সেবা, অবস্থা, আইন, অনুশীলন, উদ্দেশ্য, কারণ, সাধনা, দলিল', 'কাজ, করা, পরিচালনা করা, ক্রিয়া, ক্রীতদাস, যাওয়া, চলা, চালান, পদব্রজে ভ্রমণ, অবিরত, শ্রম, পরিশ্রম, চেষ্টা', 'কাজ, করা, পরিচালনা করা, ক্রিয়া, ক্রীতদাস, যাওয়া, চলা, চালান, পদব্রজে ভ্রমণ, অবিরত, শ্রম, পরিশ্রম, চেষ্টা', 'কাজ, কর্ম, শ্রম, চাকরি, চাকরী, সেবা, অবস্থা, আইন, অনুশীলন, উদ্দেশ্য, কারণ, সাধনা, দলিল', 'কাজ, কর্ম, শ্রম, চাকরি, চাকরী, সেবা, অবস্থা, আইন, অনুশীলন, উদ্দেশ্য, কারণ, সাধনা, দলিল', NULL, '<p>1. work :: কাজ</p><p>2. job :: কাজ</p><p>3. action :: কর্ম</p><p>4. assignment :: কাজ</p>', '<p>1. work :: কাজ</p><p>2. job :: কাজ</p><p>3. action :: কর্ম</p><p>4. assignment :: কাজ</p><p>5. labor :: শ্রম</p>', NULL, 'English to Bengali Dictionary: work<br>Meaning and definitions of work, translation in Bengali language for work with similar and opposite words. Also find spoken pronunciation of work in Bengali and in English language.<br><br>Tags for the entry \"work\"<br>What work means in Bengali, work meaning in Bengali, work definition, examples and pronunciation of work in Bengali language.', 'Work Something&nbsp;', 'Work Something&nbsp;', 'Work Something&nbsp;', 'Work Something Work Something Work Something&nbsp;', 'Work Something&nbsp;', 'Work Something&nbsp;', NULL, NULL, NULL, 'public/upload/Extraimages/1.jpg', 1, '2017-12-22', 'Solaiman Ahmed', NULL, NULL),
(23, 1, 23, 1, 'কাজ', 'public/upload/files/1.mp3', 'public/upload/images/1.png', 'কাজ, কর্ম, শ্রম, চাকরি, চাকরী, সেবা, অবস্থা, আইন, অনুশীলন, উদ্দেশ্য, কারণ, সাধনা, দলিল', 'কাজ, করা, পরিচালনা করা, ক্রিয়া, ক্রীতদাস, যাওয়া, চলা, চালান, পদব্রজে ভ্রমণ, অবিরত, শ্রম, পরিশ্রম, চেষ্টা', 'কাজ, করা, পরিচালনা করা, ক্রিয়া, ক্রীতদাস, যাওয়া, চলা, চালান, পদব্রজে ভ্রমণ, অবিরত, শ্রম, পরিশ্রম, চেষ্টা', 'কাজ, কর্ম, শ্রম, চাকরি, চাকরী, সেবা, অবস্থা, আইন, অনুশীলন, উদ্দেশ্য, কারণ, সাধনা, দলিল', 'কাজ, কর্ম, শ্রম, চাকরি, চাকরী, সেবা, অবস্থা, আইন, অনুশীলন, উদ্দেশ্য, কারণ, সাধনা, দলিল', NULL, '<p>1. work :: কাজ</p><p>2. job :: কাজ</p><p>3. action :: কর্ম</p><p>4. assignment :: কাজ</p>', '<p>1. work :: কাজ</p><p>2. job :: কাজ</p><p>3. action :: কর্ম</p><p>4. assignment :: কাজ</p><p>5. labor :: শ্রম</p>', NULL, 'English to Bengali Dictionary: work<br>Meaning and definitions of work, translation in Bengali language for work with similar and opposite words. Also find spoken pronunciation of work in Bengali and in English language.<br><br>Tags for the entry \"work\"<br>What work means in Bengali, work meaning in Bengali, work definition, examples and pronunciation of work in Bengali language.', 'Work Something&nbsp;', 'Work Something&nbsp;', 'Work Something&nbsp;', 'Work Something Work Something Work Something&nbsp;', 'Work Something&nbsp;', 'Work Something&nbsp;', NULL, NULL, NULL, 'public/upload/Extraimages/1.jpg', 1, '2017-12-22', 'Solaiman Ahmed', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `particulartemps`
--

CREATE TABLE `particulartemps` (
  `id` int(10) UNSIGNED NOT NULL,
  `word_temp_id` int(10) UNSIGNED DEFAULT NULL,
  `dictionary_id` int(10) UNSIGNED DEFAULT NULL,
  `meaning` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `audio_url` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meaning_image_url` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `noun` text COLLATE utf8mb4_unicode_ci,
  `pronoun` text COLLATE utf8mb4_unicode_ci,
  `adjective` text COLLATE utf8mb4_unicode_ci,
  `verb` text COLLATE utf8mb4_unicode_ci,
  `adverb` text COLLATE utf8mb4_unicode_ci,
  `singular` text COLLATE utf8mb4_unicode_ci,
  `plural` text COLLATE utf8mb4_unicode_ci,
  `synonym` text COLLATE utf8mb4_unicode_ci,
  `antonym` text COLLATE utf8mb4_unicode_ci,
  `origin` text COLLATE utf8mb4_unicode_ci,
  `history` text COLLATE utf8mb4_unicode_ci,
  `in_culture` text COLLATE utf8mb4_unicode_ci,
  `in_medical` text COLLATE utf8mb4_unicode_ci,
  `in_biology` text COLLATE utf8mb4_unicode_ci,
  `in_engineering` text COLLATE utf8mb4_unicode_ci,
  `in_science` text COLLATE utf8mb4_unicode_ci,
  `confusing_with` text COLLATE utf8mb4_unicode_ci,
  `related_with` text COLLATE utf8mb4_unicode_ci,
  `related_with_phrases` text COLLATE utf8mb4_unicode_ci,
  `image_url` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_published` tinyint(1) NOT NULL,
  `publish_date` date DEFAULT NULL,
  `publish_by` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `particulartemps`
--

INSERT INTO `particulartemps` (`id`, `word_temp_id`, `dictionary_id`, `meaning`, `audio_url`, `meaning_image_url`, `noun`, `pronoun`, `adjective`, `verb`, `adverb`, `singular`, `plural`, `synonym`, `antonym`, `origin`, `history`, `in_culture`, `in_medical`, `in_biology`, `in_engineering`, `in_science`, `confusing_with`, `related_with`, `related_with_phrases`, `image_url`, `is_published`, `publish_date`, `publish_by`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'কাজ', 'public/upload/files/1.mp3', 'public/upload/images/1.png', 'কাজ, কর্ম, শ্রম, চাকরি, চাকরী, সেবা, অবস্থা, আইন, অনুশীলন, উদ্দেশ্য, কারণ, সাধনা, দলিল', 'কাজ, করা, পরিচালনা করা, ক্রিয়া, ক্রীতদাস, যাওয়া, চলা, চালান, পদব্রজে ভ্রমণ, অবিরত, শ্রম, পরিশ্রম, চেষ্টা', 'কাজ, করা, পরিচালনা করা, ক্রিয়া, ক্রীতদাস, যাওয়া, চলা, চালান, পদব্রজে ভ্রমণ, অবিরত, শ্রম, পরিশ্রম, চেষ্টা', 'কাজ, কর্ম, শ্রম, চাকরি, চাকরী, সেবা, অবস্থা, আইন, অনুশীলন, উদ্দেশ্য, কারণ, সাধনা, দলিল', 'কাজ, কর্ম, শ্রম, চাকরি, চাকরী, সেবা, অবস্থা, আইন, অনুশীলন, উদ্দেশ্য, কারণ, সাধনা, দলিল', NULL, '<p>1. work :: কাজ</p><p>2. job :: কাজ</p><p>3. action :: কর্ম</p><p>4. assignment :: কাজ</p>', '<p>1. work :: কাজ</p><p>2. job :: কাজ</p><p>3. action :: কর্ম</p><p>4. assignment :: কাজ</p><p>5. labor :: শ্রম</p>', NULL, 'English to Bengali Dictionary: work<br>Meaning and definitions of work, translation in Bengali language for work with similar and opposite words. Also find spoken pronunciation of work in Bengali and in English language.<br><br>Tags for the entry \"work\"<br>What work means in Bengali, work meaning in Bengali, work definition, examples and pronunciation of work in Bengali language.', 'Work Something&nbsp;', 'Work Something&nbsp;', 'Work Something&nbsp;', 'Work Something Work Something Work Something&nbsp;', 'Work Something&nbsp;', 'Work Something&nbsp;', NULL, NULL, NULL, 'public/upload/Extraimages/1.jpg', 2, '2017-12-22', 'Solaiman Ahmed', NULL, NULL),
(2, 5, 1, 'Quit', 'public/upload/files/2.mp3', 'public/upload/images/2.png', 'Quit', 'Quit', 'Quit', 'Quit', 'Quit', 'Quit', NULL, NULL, NULL, 'Done', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2017-10-10', 'Default', NULL, NULL),
(3, 2, 1, 'বাস্তব', 'public/upload/files/3.mp3', 'public/upload/images/3.png', 'বাস্তব, আসল, শারীরিক, সত্য, মূল, অকৃত্রিম, খাঁটি, বাস্তবিক, সারগর্ভ, কার্যকর, অধিকার, নির্ভুল, যথার্থ, আন্তরিক', NULL, 'বাস্তববাদ, বাস্তব, বাস্তবতা, প্রভাব, বাস্তবিকতা, ঐতিহাসিক সত্য, সত্য', NULL, NULL, NULL, NULL, '<p>1. real :: বাস্তব</p><p>2. tangible :: বাস্তব</p><p>3. substantive :: বাস্তব</p><p>4. actual :: আসল</p><p>5. physical :: শারীরিক</p><div><br></div>', '<p>1. bogus :: মেকি</p><p>2. counterfeit :: জাল</p><p>3. fake :: নকল</p><p>4. mock :: উপহাস</p><div><br></div>', '<p>English to Bengali Dictionary: real<br>Meaning and definitions of real, translation in Bengali language for real with similar and opposite words. Also find spoken pronunciation of real in Bengali and in English language.<br><br>Tags for the entry \"real\"<br>What real means in Bengali, real meaning in Bengali, real definition, examples and pronunciation of real in Bengali language.</p>', 'English to Bengali Dictionary: real<br>Meaning and definitions of real, translation in Bengali language for real with similar and opposite words. Also find spoken pronunciation of real in Bengali and in English language.<br><br>Tags for the entry \"real\"<br>What real means in Bengali, real meaning in Bengali, real definition, examples and pronunciation of real in Bengali language.', '<p>6. cereal bowl :: খাদ্যশস্য বাটি</p><p>7. cereal box :: খাদ্যশস্য বক্স</p><p>8. cereal crops :: খাদ্যশস্য ফসলের</p><p>9. cereal grain :: শস্য দানা</p>', '<p>1. arboreal salamander :: বৃক্ষবাসী স্যালামাণ্ডার</p><p>2. augmented reality :: উদ্দীপিত বাস্তবতা</p><p>3. be realistic :: বাস্তববাদী হও</p><p>4. boreal forest :: উত্তরের বনের</p><div><br></div>', '<p>1. arboreal salamander :: বৃক্ষবাসী স্যালামাণ্ডার</p><p>2. augmented reality :: উদ্দীপিত বাস্তবতা</p><p>3. be realistic :: বাস্তববাদী হও</p><p>4. boreal forest :: উত্তরের বনের</p><div><br></div>', '<p>1. arboreal salamander :: বৃক্ষবাসী স্যালামাণ্ডার</p><p>2. augmented reality :: উদ্দীপিত বাস্তবতা</p><p>3. be realistic :: বাস্তববাদী হও</p><p>4. boreal forest :: উত্তরের বনের</p><div><br></div>', '<p>1. arboreal salamander :: বৃক্ষবাসী স্যালামাণ্ডার</p><p>2. augmented reality :: উদ্দীপিত বাস্তবতা</p><p>3. be realistic :: বাস্তববাদী হও</p><p>4. boreal forest :: উত্তরের বনের</p><div><br></div>', NULL, NULL, NULL, 'public/upload/Extraimages/3.png', 1, '2017-10-10', 'Default', NULL, NULL),
(4, 3, 1, 'Wash  Wash', 'public/upload/files/4.mp3', 'public/upload/images/4.jpg', 'Wash', 'Wash', 'Wash', 'Wash', NULL, 'Wash', NULL, NULL, 'Wash', 'wwwwwwwww', 'Wash Washing', 'Wash', 'Wash', NULL, 'Wash', 'Wash', NULL, NULL, NULL, 'public/upload/Extraimages/4.png', 1, '2017-10-10', 'Default', NULL, NULL),
(5, 7, 1, 'হৃদয়', NULL, NULL, 'হৃদয়, বক্ষ, স্তন, মন, মনোযোগ, আত্মা, চিন্তা, স্মৃতি, অন্তর, শেষ, ভিতরে, গভীরতা, আন্তরিকতা, সাহস', 'হৃদয়', NULL, NULL, NULL, NULL, NULL, '<p>1. heart :: হৃদয়</p><p>2. bosom :: বক্ষ</p><p>3. breast :: স্তন</p><p>4. mind :: মন</p>', '1. mercilessness :: নির্মমতা', 'English to Bengali Dictionary: heart<br>Meaning and definitions of heart, translation in Bengali language for heart with similar and opposite words. Also find spoken pronunciation of heart in Bengali and in English language.<br><br>Tags for the entry \"heart\"<br>What heart means in Bengali, heart meaning in Bengali, heart definition, examples and pronunciation of heart in Bengali language.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL),
(7, 5, 1, 'Quit nnnnnn', 'public/upload/files/7.mp3', 'public/upload/images/7.png', 'Quit', 'Quit', 'Quit', 'Quit', 'Quit', 'Quit', 'Quit Quit Quit', NULL, 'Quit Quit', 'Quit', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2017-10-10', 'Default', NULL, NULL),
(9, 7, 1, 'Heart Heart', 'public/upload/files/9.mp3', 'public/upload/images/9.png', 'Heart', 'Heart', 'Heart', NULL, 'Heart', NULL, NULL, NULL, NULL, 'Heart&nbsp;Heart&nbsp;Heart', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `role_name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 1, '2017-12-02 18:00:00', NULL),
(2, 'Oparator', 1, '2017-12-02 18:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(56) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` int(10) UNSIGNED DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `updated_by` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `blocked_by` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `blocked_date` date DEFAULT NULL,
  `blocked_reason` text COLLATE utf8mb4_unicode_ci,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `phone`, `role_id`, `status`, `updated_by`, `blocked_by`, `blocked_date`, `blocked_reason`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Solaiman Ahmed', 'solaiman@gmail.com', '$2y$10$upewoKRn5m8S8F7EDIMIv.2UPzNtPlwe9njiFFMv.4I4V/hkWqs3q', '01939835383', 1, 1, NULL, NULL, NULL, NULL, 'dRRWkXlYoPWUo9OfzdKFOrJGJKSSg1hecEVv0RFbaOmuG4wzkUIIBnxxfUAu', '2017-12-06 10:31:42', NULL),
(2, 'Saiful Islam', 'saiful@gmail.com', '$2y$10$upewoKRn5m8S8F7EDIMIv.2UPzNtPlwe9njiFFMv.4I4V/hkWqs3q', '01715768790', 1, 1, NULL, NULL, NULL, NULL, 'OFzZx3gKXF9ONmMJBKIMto0xB3Uklz5SGJRpFUyBAVNwf3a1q0GuGCK1UTk5', '2017-12-06 10:31:42', NULL),
(3, 'Tanveer Hasan', 'tanveer@gmail.com', '$2y$10$upewoKRn5m8S8F7EDIMIv.2UPzNtPlwe9njiFFMv.4I4V/hkWqs3q', '01939835383', 2, 1, NULL, NULL, NULL, NULL, 'uPwwyItTEi1l4P4xCBfJ9etgygoendoJaq9ck8A8GvNz6QaJybeImOPVXkSZ', '2017-12-07 05:26:24', NULL),
(4, 'Rayhan Islam', 'rayhan@gmail.com', '$2y$10$upewoKRn5m8S8F7EDIMIv.2UPzNtPlwe9njiFFMv.4I4V/hkWqs3q', '01715768790', 2, 1, NULL, NULL, NULL, NULL, 'OFzZx3gKXF9ONmMJBKIMto0xB3Uklz5SGJRpFUyBAVNwf3a1q0GuGCK1UTk5', '2017-12-07 05:26:24', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `words`
--

CREATE TABLE `words` (
  `id` int(10) UNSIGNED NOT NULL,
  `word_temp_id` int(10) UNSIGNED DEFAULT NULL,
  `dictionary_id` int(10) UNSIGNED DEFAULT NULL,
  `group_id` int(10) UNSIGNED DEFAULT NULL,
  `dictionary_word` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pronouciation_url` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `words`
--

INSERT INTO `words` (`id`, `word_temp_id`, `dictionary_id`, `group_id`, `dictionary_word`, `type`, `pronouciation_url`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, 'Work', 'Verb', NULL, '2017-12-06 04:38:26', '2017-12-06 04:38:26'),
(2, 2, 1, NULL, 'Real', 'Adverb', NULL, '2017-12-07 04:48:53', '2017-12-07 04:48:53'),
(3, 3, 1, NULL, 'wash', 'Adverb', NULL, '2017-12-10 04:38:05', '2017-12-10 04:38:05'),
(4, 3, 1, NULL, 'wash', 'Adverb', NULL, '2017-12-21 00:36:03', '2017-12-21 00:36:03'),
(5, 1, 1, NULL, 'Work', 'Verb', NULL, '2017-12-21 00:51:13', '2017-12-21 00:51:13'),
(6, 3, 1, NULL, 'wash', 'Adverb', NULL, '2017-12-21 00:52:11', '2017-12-21 00:52:11'),
(7, 1, 1, NULL, 'Work', 'Verb', NULL, '2017-12-21 00:56:17', '2017-12-21 00:56:17'),
(8, 1, 1, NULL, 'Work', 'Verb', NULL, '2017-12-21 00:58:27', '2017-12-21 00:58:27'),
(9, 1, 1, NULL, 'Work', 'Verb', NULL, '2017-12-21 00:59:29', '2017-12-21 00:59:29'),
(10, 1, 1, NULL, 'Work', 'Verb', NULL, '2017-12-21 01:00:37', '2017-12-21 01:00:37'),
(11, 1, 1, NULL, 'Work', 'Verb', NULL, '2017-12-21 01:14:48', '2017-12-21 01:14:48'),
(12, 1, 1, NULL, 'Work', 'Verb', NULL, '2017-12-21 01:20:49', '2017-12-21 01:20:49'),
(13, 3, 1, NULL, 'wash', 'Adverb', NULL, '2017-12-21 02:18:08', '2017-12-21 02:18:08'),
(14, 1, 1, NULL, 'Work', 'Verb', NULL, '2017-12-21 02:21:26', '2017-12-21 02:21:26'),
(15, 1, 1, NULL, 'Work', 'Verb', NULL, '2017-12-21 02:23:36', '2017-12-21 02:23:36'),
(16, 1, 1, NULL, 'Work', 'Verb', NULL, '2017-12-21 02:33:25', '2017-12-21 02:33:25'),
(17, 3, 1, NULL, 'wash', 'Adverb', NULL, '2017-12-21 03:03:07', '2017-12-21 03:03:07'),
(18, 1, 1, NULL, 'Work', 'Verb', NULL, '2017-12-21 06:49:42', '2017-12-21 06:49:42'),
(19, 7, 1, NULL, 'Heart', 'Pronoun', NULL, '2017-12-21 07:01:33', '2017-12-21 07:01:33'),
(20, 2, 1, NULL, 'Real', 'Adverb', NULL, '2017-12-21 07:02:34', '2017-12-21 07:02:34'),
(21, 1, 1, NULL, 'Work', 'Verb', NULL, '2017-12-21 07:11:42', '2017-12-21 07:11:42'),
(22, 1, 1, NULL, 'Work', 'Verb', NULL, '2017-12-22 03:09:05', '2017-12-22 03:09:05'),
(23, 1, 1, NULL, 'Work', 'Verb', NULL, '2017-12-22 03:13:32', '2017-12-22 03:13:32');

-- --------------------------------------------------------

--
-- Table structure for table `wordtemps`
--

CREATE TABLE `wordtemps` (
  `id` int(10) UNSIGNED NOT NULL,
  `dictionary_id` int(10) UNSIGNED DEFAULT NULL,
  `group_id` int(10) UNSIGNED DEFAULT NULL,
  `dictionary_word` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `audio_url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `create_by` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_imported` tinyint(1) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `import_date` date DEFAULT NULL,
  `import_by` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wordtemps`
--

INSERT INTO `wordtemps` (`id`, `dictionary_id`, `group_id`, `dictionary_word`, `type`, `audio_url`, `create_by`, `is_imported`, `status`, `import_date`, `import_by`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, 'Work', 'Verb', NULL, 'defaultName', 2, 2, '2017-10-10', 'defaultName', NULL, NULL),
(2, 1, NULL, 'Real', 'Adverb', NULL, 'defaultName', 1, 4, '2017-10-10', 'defaultName', NULL, NULL),
(3, 1, NULL, 'wash', 'Adverb', NULL, 'defaultName', 1, 4, '2017-10-10', 'defaultName', NULL, NULL),
(5, 1, NULL, 'Quit', 'Adjective', NULL, 'defaultName', 1, 3, '2017-10-10', 'defaultName', NULL, NULL),
(6, 1, 1, 'Rose', 'Noun', NULL, 'defaultName', 1, 1, '2017-10-10', 'defaultName', NULL, NULL),
(7, 1, 2, 'Heart', 'Pronoun', NULL, 'defaultName', 1, 3, '2017-10-10', 'defaultName', NULL, NULL),
(9, 1, NULL, 'beach', 'Noun', NULL, 'defaultName', 1, 1, '2017-10-10', 'defaultName', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dictionaries`
--
ALTER TABLE `dictionaries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `dictionaries_language_from_id_foreign` (`language_from_id`),
  ADD KEY `dictionaries_language_to_id_foreign` (`language_to_id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `particulars`
--
ALTER TABLE `particulars`
  ADD PRIMARY KEY (`id`),
  ADD KEY `particulars_particulartemp_id_foreign` (`particulartemp_id`),
  ADD KEY `particulars_word_id_foreign` (`word_id`),
  ADD KEY `particulars_dictionary_id_foreign` (`dictionary_id`);

--
-- Indexes for table `particulartemps`
--
ALTER TABLE `particulartemps`
  ADD PRIMARY KEY (`id`),
  ADD KEY `particulartemps_dictionary_id_foreign` (`dictionary_id`),
  ADD KEY `particulartemps_word_temp_id_foreign` (`word_temp_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Indexes for table `words`
--
ALTER TABLE `words`
  ADD PRIMARY KEY (`id`),
  ADD KEY `words_word_temp_id_foreign` (`word_temp_id`),
  ADD KEY `words_dictionary_id_foreign` (`dictionary_id`),
  ADD KEY `words_group_id_foreign` (`group_id`);

--
-- Indexes for table `wordtemps`
--
ALTER TABLE `wordtemps`
  ADD PRIMARY KEY (`id`),
  ADD KEY `wordtemps_dictionary_id_foreign` (`dictionary_id`),
  ADD KEY `wordtemps_group_id_foreign` (`group_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `dictionaries`
--
ALTER TABLE `dictionaries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `particulars`
--
ALTER TABLE `particulars`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `particulartemps`
--
ALTER TABLE `particulartemps`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `words`
--
ALTER TABLE `words`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `wordtemps`
--
ALTER TABLE `wordtemps`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `dictionaries`
--
ALTER TABLE `dictionaries`
  ADD CONSTRAINT `dictionaries_language_from_id_foreign` FOREIGN KEY (`language_from_id`) REFERENCES `languages` (`id`),
  ADD CONSTRAINT `dictionaries_language_to_id_foreign` FOREIGN KEY (`language_to_id`) REFERENCES `languages` (`id`);

--
-- Constraints for table `particulars`
--
ALTER TABLE `particulars`
  ADD CONSTRAINT `particulars_dictionary_id_foreign` FOREIGN KEY (`dictionary_id`) REFERENCES `dictionaries` (`id`),
  ADD CONSTRAINT `particulars_particulartemp_id_foreign` FOREIGN KEY (`particulartemp_id`) REFERENCES `particulartemps` (`id`),
  ADD CONSTRAINT `particulars_word_id_foreign` FOREIGN KEY (`word_id`) REFERENCES `words` (`id`);

--
-- Constraints for table `particulartemps`
--
ALTER TABLE `particulartemps`
  ADD CONSTRAINT `particulartemps_dictionary_id_foreign` FOREIGN KEY (`dictionary_id`) REFERENCES `dictionaries` (`id`),
  ADD CONSTRAINT `particulartemps_word_temp_id_foreign` FOREIGN KEY (`word_temp_id`) REFERENCES `wordtemps` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Constraints for table `words`
--
ALTER TABLE `words`
  ADD CONSTRAINT `words_dictionary_id_foreign` FOREIGN KEY (`dictionary_id`) REFERENCES `dictionaries` (`id`),
  ADD CONSTRAINT `words_group_id_foreign` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`),
  ADD CONSTRAINT `words_word_temp_id_foreign` FOREIGN KEY (`word_temp_id`) REFERENCES `wordtemps` (`id`);

--
-- Constraints for table `wordtemps`
--
ALTER TABLE `wordtemps`
  ADD CONSTRAINT `wordtemps_dictionary_id_foreign` FOREIGN KEY (`dictionary_id`) REFERENCES `dictionaries` (`id`),
  ADD CONSTRAINT `wordtemps_group_id_foreign` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
