@extends('dictionary.master')
@section('title')
Home
@endsection
@section('maincontent')


<div class="col-lg-9">
    <div class="content-box">
        <div class="bar text-center">
            <div class="form-group">
                 <label class="btn btn-secondary"> Choose Dictionary</label>
                    @foreach($dictionarys as $dictionary)
                    <input type="radio" name="dictionary_id" value="{{$dictionary->id}}" id="rdoDic" >{{$dictionary->dictionary_name}}
                    @endforeach                
                
            </div>
            <div class="form-inline text-center">
                <div class="form-group">
                    <label for="search_text" class="sr-only">Search</label>
                    <input type="text" class="form-control" name="search_text"  id="search_text" placeholder="Search">
            
                </div>
                <button type="button" id="btnSearch" class="btnSearch btn-info form-control" >Search</button>
            </div>
            <hr>
        </div>
        
        
        <div class="message" style="display:none;">
            <h4 style="text-align:center"> Word not found</h4>
        </div>
        

        <div id="dvParticulars" style="padding-left:20px; display:none" >
            <div id="dvWord">
                    <span >
                        <div class="pull-left" id="spWord"></div>
                        <div  id="spMeaning"></div>
                    </span>
            </div>          
            
            <div id="dvAudioUrl">
              <button>
              <img  id="spAudio" src="{{asset('public/hvendor/images/speaker1.png')}}" />
              <source id="spAudio" src="" type="video/mp4">
              </button>           
            </div>
            
             <br><hr>
            <div id="dvMeaningImage">
                <label for="spImage"></label> 
                <!-- <img  id="spImage" /> -->
                <div id="spImage"></div>
            </div>
             <br><hr>
            
            <div id="dvNoun" class="section" >
                <label for="spNoun">Noun</label> 
                <div id="spNoun"></div><hr>
            </div> 
            <div id="dvPronoun">
                <label for="spPronoun"> Pronoun</label> 
                <div id="spPronoun"></div><hr>
            </div>
            <div id="dvVerb">
                <label for="spVerb">Verb</label> 
                <div id="spVerb"></div><hr>
            </div>
            
            <div id="dvAdjective">
                <label for="spAdjective">Adective</label> 
                <p id="spAdjective"></p><hr>
            </div>
            
            <div id="dvAdverb">
                <label for="spAdverb">Adverb</label> 
                <p id="spAdverb"></p><hr>
            </div>
            <div id="dvSingular">
                <label for="spSingular">Singular</label> 
                <p id="spSingular"></p><hr>
            </div>
            <div id="dvPlural">
                <label for="spPlural">Plural</label> 
                <p id="spPlural"></p><hr>
            </div>
            <div class="panel panel-info" id="dvSynonym">
                                <div class="panel-heading">
                                    Synonyms
                                </div>
                            <p id="spSynonym" style="padding-left:20px;"> </p>
            </div>
            <div class="panel panel-info" id="dvAntonym">
                                <div class="panel-heading">
                                Antonyms
                                </div>
                            <p id="spAntonym" style="padding-left:20px;"> </p>
            </div>

            <div class="panel panel-info" id="dvCulture">
                                <div class="panel-heading">
                               Culture
                                </div>
                            <p id="spCulture" style="padding-left:20px;"> </p>
            </div>

            <div class="panel panel-info" id="dvMedical">
                                <div class="panel-heading">
                                Medical
                                </div>
                            <p id="spMedical" style="padding-left:20px;"> </p>
            </div>

            <div class="panel panel-info" id="dvBiology">
                                <div class="panel-heading">
                                Biology
                                </div>
                            <p id="spBiology" style="padding-left:20px;"> </p>
            </div>

            <div class="panel panel-info" id="dvEngineering">
                                <div class="panel-heading">
                                Engineering
                                </div>
                            <p id="spEngineering" style="padding-left:20px;"> </p>
            </div>

            <div class="panel panel-info" id="dvScience">
                                <div class="panel-heading">
                                Science
                                </div>
                            <p id="spScience" style="padding-left:20px;"> </p>
            </div>
            <div class="image" id="dvImage">
                <label for="spExImage"></label> 
                <!-- <img  id="spImage" /> -->
                <div id="spExImage"></div>
            </div>
            <div class="panel panel-info" id="dvHistory">
                                <div class="panel-heading">
                                Hisotry
                                </div>
                            <p id="spHistory" style="padding-left:20px;"> </p>
            </div>
            <div class="panel panel-info" id="dvOrigin">
                                <div class="panel-heading">
                                Origin
                                </div>
                            <p id="spOrigin" style="padding-left:20px;"> </p>
            </div>
            
        </div>    
       

    </div>

    <!-- End Search !-->


</div>
<script >
$(function () {

 $('#btnSearch').click(function () {             
        var dictionary_id =$("input[name=dictionary_id]:checked").val();       
        var search_text = $("#search_text").val();
        if (dictionary_id==null || dictionary_id=='undifined')
        {
            alert("Please select dictionary");
            return;
        }
        $.ajax({
            type: 'get',
            url: '{{URL::to("/search-text")}}',
            data: { 'search': search_text,
                   'dictionary_id':dictionary_id 
                   },
            success: function (response) {               
                if(response.length==0){
                    $("#dvParticulars").hide();
                    $(".message").show();
                }else{
                    $("#dvParticulars").show();                 
                    $(".message").hide();

                    var word =response[0];
                    // console.log(word);
                    $("#dvAudioUrl").hide();
                    $("#dvMeaningImage").hide();
                    $("#dvImage").hide();
                    $("#dvNoun").hide();
                    $("#dvPronoun").hide();
                    // $("#dvVerb").hide();
                    $("#dvAdjective").hide();
                    $("#dvAdverb").hide();
                    $("#dvSingular").hide();
                    $("#dvPlural").hide();
                    $("#dvSynonym").hide();
                    $("#dvAntonym").hide();
                    $("#dvCulture").hide();                    
                    $("#dvMedical").hide();
                    $("#dvBiology").hide();
                    $("#dvEngineering").hide();
                    $("#dvScience").hide();
                    $("#dvHistory").hide();
                    $("#dvOrigin").hide();
                   
                    if(word.audio_url!=null) $("#dvAudioUrl").show();
                    if(word.meaning_image_url!=null) $("#dvMeaningImage").show();
                    if(word.image_url!=null) $("#dvImage").show();
                    if(word.noun!=null) $("#dvNoun").show();
                    if(word.pronoun!=null) $("#dvPronoun").show();
                    // if(word.verb!=null) $("#dvVerb").show();
                    if(word.adjective!=null) $("#dvAdjective").show();
                    if(word.adverb!=null) $("#dvAdverb").show();
                    if(word.singular!=null) $("#dvSingular").show();
                    if(word.plural!=null) $("#dvPlural").show();
                    if(word.synonym!=null) $("#dvSynonym").show();
                    if(word.antonym!=null) $("#dvAntonym").show();                   
                    if(word.in_culture!=null) $("#dvCulture").show();
                    if(word.in_medical!=null) $("#dvMedical").show();
                    if(word.in_biology!=null) $("#dvBiology").show();
                    if(word.in_engineering!=null) $("#dvEngineering").show();
                    if(word.in_science!=null) $("#dvScience").show();
                    if(word.history!=null) $("#dvHistory").show();
                    if(word.origin!=null) $("#dvOrigin").show();

                    $("#spWord").html(word.dictionary_word);
                    $("#spMeaning").html(word.meaning);
                    $("#spImage").html('<img src="' + word.meaning_image_url + '"width=160 height=160/>');
                    $("#spExImage").html('<img src="' + word.image_url + '"width=160 height=160/>');
                    $("#spAudio").html('<source src="http://localhost:8080/DictionaryBook/' + word.audio_url+ '" type="audio/mpeg" />');
                    // alert(word.audio_url);
                    $("#spNoun").html(word.noun);
                    $("#spVerb").html(word.verb);
                    $("#spPronoun").html(word.pronoun);
                    $("#spAdjective").html(word.adjective);
                    $("#spAdverb").html(word.adverb);
                    $("#spSingular").html(word.singular);
                    $("#spPlural").html(word.plural);
                    $("#spSynonym").html(word.synonym);
                    $("#spAntonym").html(word.antonym);
                    $("#spOrigin").html(word.origin);                  
                    $("#spCulture").html(word.in_culture);                
                    $("#spMedical").html(word.in_medical);                
                    $("#spBiology").html(word.in_biology);                
                    $("#spEngineering").html(word.in_engineering);                
                    $("#spScience").html(word.in_science);                
                    $("#spHistory").html(word.history);              
                    
                }
            }
        });
    });  
});
</script>

@endsection




