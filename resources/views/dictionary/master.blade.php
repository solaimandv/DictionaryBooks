

<!DOCTYPE HTML>
<html>
<head>
<title>@yield('title')</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Dictionarybooks" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href="{{asset('public/hvendor/css/bootstrap.min.css')}}" rel='stylesheet' type='text/css' />
<!-- Custom Theme files -->
<link href="{{asset('public/hvendor/css/style.css')}}" rel='stylesheet' type='text/css' />
<link href="{{asset('public/hvendor/css/font-awesome.css')}}" rel="stylesheet"> 
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css"/>

<script src="{{asset('public/hvendor/js/jquery-3.2.1.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script> 


<script src="{{asset('public/hvendor/js/Home.js')}}"></script>
<!-- {{ csrf_field() }} -->

</head>
<body>
<div id="wrapper">


      @include('dictionary.includes.header')
        <!--content-->
        <div class="container-fluid" style="min-height:400px;">
          <div class="row">
            @yield('maincontent')
            @include('dictionary.includes.sidebar')  
          </div>
       </div>

		@include('dictionary.includes.footer')
      

	

  <script src="{{asset('public/hvendor/js/bootstrap.min.js')}}"></script>

</body>
</html>

