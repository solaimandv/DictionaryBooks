<!--banner-->
<div class="container-fluid">
    <div class="top-header" >
        <div class=" col-md-2  ">
            <img class="img-responsive" src="{{asset('public/hvendor/images/DictionaryBook.png')}}" alt="logo">
        </div>
        <div class=" col-md-8 ">
            <!-- <img class="img-responsive" src="{{asset('public/hvendor/images/b3.jpg')}}" alt="b3" width="100%"> -->
        </div>
        <div class=" col-md-2 ">
            <ul class="nav navbar-nav  ">
            <li><a href="{{url('/login')}}"><span <i class="fa fa-sign-in" aria-hidden="true"></i></span> Login</a></li>
            </ul>
        </div>
    </div>
</div>
<nav class="navbar navbar-default alert-info">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">DictionaryBook</a>
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#my_menu">
                <span class="icon-bar"> </span>
                <span class="icon-bar"> </span>
                <span class="icon-bar"> </span>
            </button>
        </div>
        <div id="my_menu" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="#">Home</a></li>
                <li><a href="#">Dictionary</a></li>
                <li><a href="#">Category</a></li>
                <li><a href="#">Grammer</a></li>
                <li><a href="#">Browse</a></li>
                <li><a href="#">Instruction</a></li>
                <li><a href="#">Proverb</a></li>
            </ul>
        </div>
    </div>
</nav>
<!--//banner-->
