<div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                    @if(Auth::user()->role_id==2)
                        <li>
                            <a href="{{url('/word-entry')}}"><i class="fa fa-file-word-o fa-fw"></i> Word Temp Entry</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-wrench fa-fw"></i> Particular<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                   <a href="{{url('/particular-entry')}}"><i class="fa fa-table fa-fw"></i> Temp Entry</a>
                                </li>

                                <li>
                                    <a href="{{url('/view-particular')}}"><i class="fa fa-table fa-fw"></i>Temp Manage</a>
                                </li>
                               
                            </ul>
                           
                        </li>                       
                        
                        <li>
                          <a href="{{url('/particular-extra')}}"><i class="fa fa-table fa-fw"></i>Extra Info</a>
                        </li>                            
                               
                            
                       @else
                       <li>
                            <a href="{{url('/word-entry')}}"><i class="fa fa-file-word-o fa-fw"></i> Word Temp Entry</a>
                        </li>
                        <li>
                            <a href="{{url('/search-word')}}"><i class="fa fa-file-word-o fa-fw"></i>Review Word </a>
                        </li>
   
                        <li>
                            <a href="#"><i class="fa fa-wrench fa-fw"></i> Particular<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                   <a href="{{url('/particular-entry')}}"><i class="fa fa-table fa-fw"></i> Temp Entry</a>
                                </li>

                                <li>
                                    <a href="{{url('/view-particular')}}"><i class="fa fa-table fa-fw"></i>Temp Manage</a>
                                </li>
                               
                            </ul>
                           
                        </li>
                      
                        <li>
                          <a href="{{url('/particular-extra')}}"><i class="fa fa-table fa-fw"></i>Extra Info</a>
                        </li> 

                        <li>
                            <a href="#"><i class="fa fa-wrench fa-fw"></i> Security<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="panels-wells.html">Roles</a>
                                </li>
                                <li>
                                    <a href="buttons.html">User Entry</a>
                                </li>
                                @endif
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>