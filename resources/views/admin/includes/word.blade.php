<table class="table table-striped table-hover" >
<thead>
    <tr>
        <th>#</th>
        <th>Action</th>
        <th>Dictionary</th>
        <th>Word Name</th>
        <th>Word Type</th>
        
    </tr>
</thead>
<tbody>

@foreach($wordtemps as $wordtemp)
<tr class="">
    <td></td>
    <td>
    <button type="button">
    <a href="{{url('/edit-wordstemp/'.$wordtemp->id)}}"><i class="fa fa-pencil-square"  aria-hidden="true" title="Edit"></i></a></button>
    <button type="button" onclick="return confirm('Are you sure to delete the record?');">
    <a href="{{url('/delete-wordstemp/'.$wordtemp->id)}}"><i class="fa fa-trash" aria-hidden="true" title="Delete" ></i></a></button>
    </td>
    <td>{{$wordtemp->dictionary_name}}</td>
    <td>{{$wordtemp->dictionary_word}}</td>
    <td>{{$wordtemp->type}}</td>
   
   
</tr>


@endforeach

</tbody>

</table>
{{$wordtemps->links()}} 