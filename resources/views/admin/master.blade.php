

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="author" content="">

    <title>@yield('title')</title>

    <link href="{{asset('public/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('public/vendor/dist/css/sb-admin-2.css')}}" rel="stylesheet">
    <link href="{{asset('public/vendor/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('public/vendor/datatables-plugins/dataTables.bootstrap.css')}}" rel="stylesheet"> 
    <link href="{{asset('public/vendor/editor/bootstrap3-wysihtml5.min.css')}}" rel="stylesheet"> 
    <script src="{{asset('public/vendor/jquery/jquery.min.js')}}"></script>
    <script type='text/javascript' src='https://ajax.googleapis.com/ajax/libs/angularjs/1.6.6/angular.min.js'></script>
    <script type='text/javascript' src='https://ajax.googleapis.com/ajax/libs/angularjs/1.6.6/angular-sanitize.min.js'></script>


</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">DictionaryBooks.com</a>
            </div>
            <!-- /.navbar-header -->

           @include('admin.includes.header')
            <!-- /.navbar-top-links -->

            @include('admin.includes.sidebar')
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
         @yield('content')
            <!-- /.row -->
            
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
    <div class="footer pull-right">
        sdfsdf
    </div>

    <!-- jQuery -->
 
    <script src="{{asset('public/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('public/vendor/dist/js/sb-admin-2.js')}}"></script>
    <script src="{{asset('public/vendor/metisMenu/metisMenu.js')}}"></script>
    <script src="{{asset('public/vendor/datatables/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('public/vendor/datatables-plugins/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('public/vendor/editor/bootstrap3-wysihtml5.all.min.js')}}"></script>
   
   
   
    
    
    
    
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
    </script>
  
<script type="text/javascript">
	
    $('#txtNoun').wysihtml5();
    $('#txtPronoun').wysihtml5();
    $('#txtAdjective').wysihtml5();
    $('#txtVerb').wysihtml5();
    $('#txtAdverb').wysihtml5();
    $('#txtSingular').wysihtml5();
    $('#txtPlural').wysihtml5();
    $('#txtSynonym').wysihtml5();
    $('#txtAntonym').wysihtml5();
    $('#txtOrigin').wysihtml5();

    $('#txtCulture').wysihtml5();
    $('#txtMedical').wysihtml5();
    $('#txtBiology').wysihtml5();
    $('#txtScience').wysihtml5();
    $('#txtEngineering').wysihtml5();
    $('#txtHistory').wysihtml5();
    
    
   
</script>

<script>
 $(document).on('click', '.pagination a', function (e) {
            
            e.preventDefault();
            var url=$(this).attr('href').split('page=')[1];
            // console.log(url);
          $.ajax({
               url:'/ajax-pagination?page='+ url
            
          })
         
        });
</script>
<script>


</script>

</body>

</html>
