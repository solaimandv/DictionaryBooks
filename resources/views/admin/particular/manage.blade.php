@extends('admin.master')
@section('title')
Particular View
@endsection
@section('content')

            <div class="container-fluid">
              <h3 style="color:green">{{Session::get("message")}}</h3>
        
                    <div class="panel panel-default">
                            <div class="panel-heading text-center">
                                Particular Temp List
                            </div>
                        <div class="form-row">

                            <div class="col-md-4">
                                <label for="dictionary_id">Dictionary</label>
                                <select class="custom-select form-control" name="dictionary_id"  id="dictionary_id" required>
                                    <option value="">Select Dictionary</option>
                                    @foreach($dictionarys as $dictionary)
                                    <option value="{{$dictionary->id}}">{{$dictionary->dictionary_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-4">
                                <label for="word_temp_id"> Status:</label>

                                <select class="custom-select form-control" name="word_temp_id"  id="status" required>
                                    <option value="">Select Status</option>
                                    <option value="1">New</option>                       
                                    <option value="3">To Review</option>

                                </select>
                            </div>
                            <div class="col-md-4">
                                <label for="dictionaryWord">Word Name:</label>

                                <select class="custom-select form-control" name="word_temp_id" id="word_temp_id"  required>
                                    <option value="" >Select Word</option>
                                
                                </select>
                            </div>

                        </div>
                            <!-- /.panel-heading -->
                        <div class="col-lg-12">
                            <div class="panel-body">
                                <div class="table-responsive">
                                <table width="100%" class="table table-striped table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Action</th>
                                                <th> Dictionry</th>
                                                <th>Word</th>
                                                <th>Meaning</th>
                                                <th>Noun</th>
                                                <th>Pronoun</th>
                                                <th>Adjective</th>
                                                <th>verb</th>
                                                <th>Adverb</th>
                                                <th>Singular</th>
                                                <th>Plural</th>
                                                <th>Synonym</th>
                                                <th>Antonym</th>
                                                <th>Culture</th>
                                                <th>Medical</th>
                                                <th>Biology</th>
                                                <th>Engineering</th>
                                                <th>Science</th>
                                                <th>Origin</th>
                                                <th>History</th>
                                                <th>is_published</th>
                                                <th>publish_date</th>
                                                <th>publish_by</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody id="generalData">
                                        <?php $i=1?>
                                        @foreach($particularTemp as $data)
                                            <tr>
                                                <td>{{$i}}</td>
                                                <td>
                                            
                                            <a class="btn btn-primary" href="{{url('/edit-particular/'.$data->id)}}"><i class="fa fa-pencil-square"  aria-hidden="true" title="Edit"></i></a>
                                            
                                            <a class="btn btn-primary" onclick="return confirm('Are you sure to delete the record?');" href="{{url('/delete-particular/'.$data->id)}}"><i class="fa fa-trash" aria-hidden="true" title="Delete" ></i></a>
                                            
                                            
                                            </td>
                                                <td>{{$data->dictionary_name}}</td>
                                                <td>{{$data->dictionary_word}}</td>
                                                <td>{{$data->meaning}}</td>
                                                <td>{!!$data->noun!!}</td>
                                                <td>{!!$data->pronoun!!}</td>
                                                <td>{!!$data->adjective!!}</td>
                                                <td>{!!$data->verb!!}</td>
                                                <td>{!!$data->adverb!!}</td>
                                                <td>{!!$data->singular!!}</td>
                                                <td>{!!$data->plural!!}</td>
                                                <td>{!!$data->synonym!!}</td>
                                                <td>{!!$data->antonym!!}</td>
                                                <td>{!!$data->in_culture!!}</td>
                                                <td>{!!$data->in_medical!!}</td>
                                                <td>{!!$data->in_biology!!}</td>
                                                <td>{!!$data->in_engineering!!}</td>
                                                <td>{!!$data->in_science!!}</td>                                            
                                                <td>{!!$data->origin!!}</td>                                            
                                                <td>{!!$data->history!!}</td>                                            
                                                <td>{!!$data->is_published!!}</td>
                                                <td>{!!$data->publish_date!!}</td>
                                                <td>{!!$data->publish_by!!}</td>
                                                
                                            </tr>
                                            <?php $i++?>
                                            @endforeach
                                        </tbody>
                                        <tbody id="ajaxdata">
                                        
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.table-responsive -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                    </div>
                    <!-- /.panel -->
            </div>
            <script>
$(document).ready(function(){    
    function loadWord(dictionaryId,status){
        if(dictionaryId==null) return;
        if(status==null) return;
    $.ajax({
                    url: "{{url('/search/word/')}}/"+dictionaryId+"/"+status+"/",
                    type: "GET",      
                    success:function(data) {                       
                    $('#word_temp_id').empty();
                    $.each(data, function(key, value) {
                        $('#word_temp_id').append('<option value="'+ value.id +'">'+value.dictionary_word +'</option>');
                        });
                    }
                });
}      

    $('#dictionary_id').change(function(){
    var dictionaryId=$('#dictionary_id').val();
    var status=$('#status').val();
    if(dictionaryId>0 && status>0){
        loadWord(dictionaryId,status);
     }
 });

 $('#status').change(function(){
var dictionaryId=$('#dictionary_id').val();
var status=$('#status').val();
if(dictionaryId>0 && status>0){
    loadWord(dictionaryId, status);
}
});
    $('#word_temp_id').change(function(){
        var dictionaryId=$('#dictionary_id').val();
        var status=$('#status').val();
        var id=$(this).val();      
        $.ajax({        
            url: "{{url('/particular-view/word')}}/"+dictionaryId+"/"+status+"/"+id+"/",
            type: "GET",                       
            dataType: "json",
            success: function (data) {                
                if(data)
                {   
                    var	rows = '';
                    $('#generalData').hide();
                    $('#ajaxdata').show();                   
                    rows = rows + '<tr>';                     
                        rows = rows + '<td>'+data.id+'</td>';
                        rows = rows + '<td> <a class="btn btn-primary" href="{{url('/edit-particular/')}}/'+data.id+'" ><i class="fa fa-pencil-square"  aria-hidden="true" title="Edit"></i></a><a class="btn btn-danger"   href="{{url('/delete-particular/')}}/'+data.id+'" ><i class="fa fa-trash"  aria-hidden="true" title="Delete"></i></a></td>';
                        rows = rows + '<td>'+data.dictionary_name+'</td>';
                        rows = rows + '<td>'+data.dictionary_word+'</td>';
                        rows = rows + '<td>'+data.meaning+'</td>';
                        rows = rows + '<td>'+data.noun+'</td>';
                        rows = rows + '<td>'+data.pronoun+'</td>';
                        rows = rows + '<td>'+data.adjective+'</td>';
                        rows = rows + '<td>'+data.verb+'</td>';
                        rows = rows + '<td>'+data.adverb+'</td>';
                        rows = rows + '<td>'+data.singular+'</td>';
                        rows = rows + '<td>'+data.plural+'</td>';
                        rows = rows + '<td>'+data.synonym+'</td>';
                        rows = rows + '<td>'+data.antonym+'</td>';
                        rows = rows + '<td>'+data.in_culture+'</td>';
                        rows = rows + '<td>'+data.in_medical+'</td>';
                        rows = rows + '<td>'+data.in_biology+'</td>';
                        rows = rows + '<td>'+data.in_engineering+'</td>';
                        rows = rows + '<td>'+data.in_science+'</td>';
                        rows = rows + '<td>'+data.origin+'</td>';
                        rows = rows + '<td>'+data.history+'</td>';
                        rows = rows + '<td>'+data.is_published+'</td>';
                        rows = rows + '<td>'+data.publish_date+'</td>';                       
                        rows = rows + '</tr>';
                                        }               
                $("tbody").html(rows);
                    },                   
	        });    
       });
});
</script>
 @endsection
