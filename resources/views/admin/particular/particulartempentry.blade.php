@extends('admin.master')
@section('title')
Particular  Entry
@endsection
@section('content')

<div class="container-fluid">

        <hr>
        <h3>
        {{Session::get("message")}}
        </h3>
    <div class="col-md-10 well">
    <h4 class="text-center">Perticular Temp Entry</h4>
    {!!Form::open(['url'=>'/store/particular-temp','method'=>'POST' ,'class'=>'form-horizontal','enctype' => 'multipart/form-data'])!!}
        <div class="form-row">
            
        
            <div class="col-lg-4">
            <label for="dictionary_id">Dictionary</label>
                <select class="custom-select form-control" name="dictionary_id" id="dictionary_id"  required>
                    <option value="">Select Dictionary</option>
                    @foreach($dictionarys as $dictionary)
                        <option value="{{$dictionary->id}}">{{$dictionary->dictionary_name}}</option>
                    @endforeach
                </select>
            </div>
            <div class=" col-lg-4">
            <label for="dictionaryWord"> Status:</label>
            
            <select class="custom-select form-control" name="status" id="status"  required>
                    <option value="">Select Status</option>
                    <option value="1">New</option>                   
                    <option value="3">To Review</option>
                    
                </select>
            </div>
            <div class="col-lg-4">
            <label for="dictionaryWord">Word Name:</label>
            
            <select class="custom-select form-control" name="word_temp_id" id="word_temp_id"  required>
                    <option value="">Select Word</option>
                   
                </select>
            </div>
            
        </div>
        <div class="form-group">
           <label for="meaning">Word Meaning:</label>
           
           <input type="text" class="form-control" id="meaning" name="meaning" aria-describedby="emailHelp" placeholder="Word Meaning......" required>
           <input type="hidden" class="form-control" id="particular_id" name="id"  aria-describedby="emailHelp" placeholder="Word Meaning......" >
        </div>
   
        <div class="form-group">
            <label for="imageUrl"> Image</label>
            <input type="file" class="form-control-file" name="meaning_image_url">
            <div id="imageUrl"></div>
        </div>
        <div class="form-group">
            <label for="audio_url"> Audio</label>
            <input type="file" class="form-control-file" name="audio_url" id="audio_url">
            <div id="audio_url"></div>
        </div>

        <div class="form-group">
            <label for="txtNoun">Noun</label>
            <textarea class="form-control text-area" id="txtNoun" name="noun" style="height:auto;" ></textarea>
        </div>
        <div class="form-group">
            <label for="txtPronoun">Pronoun</label>
            <textarea class="form-control" id="txtPronoun" name="pronoun" style="height:auto;"></textarea>
        </div>
        <div class="form-group">
            <label for="txtAdjective">Adjective</label>
            <textarea class="form-control" id="txtAdjective" name="adjective" style="height:auto;"></textarea>
        </div>
        <div class="form-group">
            <label for="txtVerb">Verb</label>
            <textarea class="form-control" id="txtVerb" name="verb" style="height:auto;"></textarea>
        </div>
        <div class="form-group">
            <label for="txtAdverb">Adverb</label>
            <textarea class="form-control" id="txtAdverb" name="adverb"  style="height:auto;"></textarea>
        </div>
        <div class="form-group">
            <label for="txtSingular">Singular</label>
            <textarea class="form-control" id="txtSingular" name="singular" style="height:auto;"></textarea>
        </div>
        <div class="form-group">
            <label for="txtPlural">Plural</label>
            <textarea class="form-control" id="txtPlural" name="plural"  style="height:auto;"></textarea>
        </div>
        <div class="form-group">
            <label for="txtSynonym">Synonym</label>
            <textarea class="form-control" id="txtSynonym" name="synonym"  style="height:auto;"></textarea>
        </div>
        <div class="form-group">
            <label for="txtAntonym">Antonym</label>
            <textarea class="form-control" id="txtAntonym" name="antonym" style="height:auto;"></textarea>
        </div>
        <div class="form-group">
            <label for="txtOrigin">origin</label>
            <textarea class="form-control" id="txtOrigin" name="origin"  style="height:auto;"></textarea>
        </div>
        <input type="hidden" class="form-control" id="is_published" name="is_published" value="1" aria-describedby="emailHelp" placeholder="Word Meaning......" required>
        <input type="hidden" class="form-control" id="publish_date" name="publish_date" value="2017/10/10" aria-describedby="emailHelp" placeholder="Word Meaning......" required>
        <input type="hidden" class="form-control" id="publish_by" name="publish_by" value="Default" aria-describedby="emailHelp" placeholder="Word Meaning......" required>
         
         <input type="submit" class="btn btn-primary"></input>
     {!!Form::close()!!}
       
 </div> 
      
</div>

<script>
$(document).ready(function(){

function  loadWord(dictionaryId, status){
    if(dictionaryId==null) return;
    if(status==null) return;
    $.ajax({
                url: "{{url('/search/word/')}}/"+dictionaryId+"/"+status+"/",
                type: "GET",        
                success:function(data) {
                $('#word_temp_id').empty();
                $.each(data, function(key, value) {
                    $('#word_temp_id').append('<option value="'+ value.id +'">'+value.dictionary_word +'</option>');
                    });
                }
            });
}


$('#dictionary_id').change(function(){
var dictionaryId=$('#dictionary_id').val();
var status=$('#status').val();
if(dictionaryId>0 && status>0){
    loadWord(dictionaryId, status);
}
});

$('#status').change(function(){
var dictionaryId=$('#dictionary_id').val();
var status=$('#status').val();
if(dictionaryId>0 && status>0){
    loadWord(dictionaryId, status);
}
});


$('#word_temp_id').change(function(){
    var dictionaryId=$('#dictionary_id').val();
    var status=$('#status').val();
    var id=$(this).val();
   
if(status==3)
{
$.ajax({
    url: "particular/review-word",
    type: "GET", 
    data: { 
        'dictionary': dictionaryId, 
        'status': status, 
        'id': id
    },
    
    dataType: "json",   
    
    success: function (data) {            
        if(data)
        {           
            $('#meaning').val(data.meaning);         
            $('#particular_id').val(data.id);         
            $('#imageUrl').html('<img src="' + data.meaning_image_url + '"width=160 height=160/>');; 
            $('#audio_url').html('<audio src="' + data.audio_url + '"width=160 height=160></audio>');; 
            $('#txtNoun').data("wysihtml5").editor.setValue(data.noun);
            $('#txtPronoun').data("wysihtml5").editor.setValue(data.pronoun);
            $('#txtAdjective').data("wysihtml5").editor.setValue(data.adjective);
            $('#txtVerb').data("wysihtml5").editor.setValue(data.verb);
            $('#txtAdverb').data("wysihtml5").editor.setValue(data.adverb);
            $('#txtSingular').data("wysihtml5").editor.setValue(data.singular);
            $('#txtPlural').data("wysihtml5").editor.setValue(data.plural);
            $('#txtSynonym').data("wysihtml5").editor.setValue(data.synonym);
            $('#txtAntonym').data("wysihtml5").editor.setValue(data.antonym);
            $('#txtOrigin').data("wysihtml5").editor.setValue(data.origin);          

        }         
                    },
});


}else{
    // alert('Select Status');
}
});
});
</script>

@endsection
