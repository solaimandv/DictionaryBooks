@extends('admin.master')
@section('title')
Edit Particular
@endsection
@section('content')

<div class="container-fluid">

        <hr>
        <h3>
        {{Session::get("message")}}
        </h3>
    <div class="col-md-10 well">
    <h4 class="text-center">Perticular Temp Edit</h4>
    {!!Form::open(['url'=>'/update-particular','method'=>'POST' ,'name'=>'particuarlform' ,'class'=>'form-horizontal','enctype' => 'multipart/form-data'])!!}
        <div class="form-row">
            
        
            <div class="form-group col-lg-6">
            <label for="dictionary_id">Dictionary</label>
                <select class="custom-select form-control" name="dictionary_id" id="dictionary_id"  required>
                    <option value="">Select Dictionary</option>
                    @foreach($dictionarys as $dictionary)
                        <option value="{{$dictionary->id}}">{{$dictionary->dictionary_name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-lg-6">
            <label for="dictionaryWord">Word Name:</label>
            
            <select class="custom-select form-control" name="word_temp_id" id="dictionaryWord"  required>
                    <option value="">Select Word</option>
                    @foreach($objWord as $wordtemp)
                        <option value="{{$wordtemp->id}}">{{$wordtemp->dictionary_word}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group">
           <label for="meaning">Word Meaning:</label>
           
           <input type="text" class="form-control" id="meaning"  name="meaning" value="{{$objParticular->meaning}}" aria-describedby="emailHelp" placeholder="Word Meaning......" required>
           <input type="hidden" class="form-control" id="meaning"  name="id" value="{{$objParticular->id}}" aria-describedby="emailHelp" placeholder="Word Meaning......" required>
        </div>
        
<div class="row dark">
    <div class="col-lg-9">
   
            
            <div class="form-group">
                <label for="imageUrl"> Image</label>
                <input type="file" class="form-control-file" name="meaning_image_url" id="imageUrl">
            <img src="{{URL::asset($objParticular->meaning_image_url)}}" alt="{{URL::asset($objParticular->meaning_image_url)}}" style="width:50px; height:50px">
            </div>
            <!-- <div class="form-group">
                <label for="audio_url"> Audio</label>
                <input type="file" class="form-control-file" name="audio_url" id="audio_url">
                <img src="{{URL::asset($objParticular->audio_url)}}" alt="{{URL::asset($objParticular->audio_url)}}" style="width:50px; height:50px">
            </div> -->
        
            
    </div>
    <!-- <div class="col-lg-3">
    <input type="submit" class="btn btn-primary pull-right"></input>
  
    </div> -->
</div>



        <div class="form-group">
            <label for="txtNoun">Noun</label>
            <textarea class="form-control text-area" id="txtNoun" name="noun" value="" style="height:auto;" >{{$objParticular->noun}}</textarea>
        </div>
        <div class="form-group">
            <label for="txtPronoun">Pronoun</label>
            <textarea class="form-control" id="txtPronoun" name="pronoun" style="height:auto;">{{$objParticular->pronoun}}</textarea>
        </div>
        <div class="form-group">
            <label for="txtAdjective">Adjective</label>
            <textarea class="form-control" id="txtAdjective" name="adjective" style="height:auto;">{{$objParticular->adjective}}</textarea>
        </div>
        <div class="form-group">
            <label for="txtVerb">Verb</label>
            <textarea class="form-control" id="txtVerb" name="verb" style="height:auto;">{{$objParticular->verb}}</textarea>
        </div>
        <div class="form-group">
            <label for="txtAdverb">Adverb</label>
            <textarea class="form-control" id="txtAdverb" name="adverb"  style="height:auto;">{{$objParticular->adverb}}</textarea>
        </div>
        <div class="form-group">
            <label for="txtSingular">Singular</label>
            <textarea class="form-control" id="txtSingular" name="singular" style="height:auto;">{{$objParticular->singular}}</textarea>
        </div>
        <div class="form-group">
            <label for="txtPlural">Plural</label>
            <textarea class="form-control" id="txtPlural" name="plural"  style="height:auto;">{{$objParticular->plural}}</textarea>
        </div>
        <div class="form-group">
            <label for="txtSynonym">Synonym</label>
            <textarea class="form-control" id="txtSynonym" name="synonym"  style="height:auto;">{{$objParticular->synonym}}</textarea>
        </div>
        <div class="form-group">
            <label for="txtAntonym">Antonym</label>
            <textarea class="form-control" id="txtAntonym" name="antonym" style="height:auto;">{{$objParticular->antonym}}</textarea>
        </div>
        <div class="form-group">
            <label for="txtOrigin">Origin</label>
            <textarea class="form-control" id="txtOrigin" name="origin"  style="height:auto;">{{$objParticular->origin}}</textarea>
        </div>
        <input type="hidden" class="form-control" id="is_published" name="is_published" value="1" aria-describedby="emailHelp" placeholder="Word Meaning......" required>
        <input type="hidden" class="form-control" id="publish_date" name="publish_date" value="2017/10/10" aria-describedby="emailHelp" placeholder="Word Meaning......" required>
        <input type="hidden" class="form-control" id="publish_by" name="publish_by" value="Default" aria-describedby="emailHelp" placeholder="Word Meaning......" required>
         
         <input type="submit" class="btn btn-primary"></input>
     {!!Form::close()!!}
       
 </div>
    

    
   
</div>
<script>
document.forms['particuarlform'].elements['dictionary_id'].value={{$objParticular->dictionary_id}}
document.forms['particuarlform'].elements['word_temp_id'].value={{$objParticular->word_temp_id}}

</script>
@endsection
