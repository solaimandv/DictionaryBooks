@extends('admin.master') @section('title') Particular Extra @endsection @section('content')


    <div class="container-fluid">

        <hr>
        <h3>
            {{Session::get("message")}}
            </h3>
        <div class="col-md-10 well">
            <h4 class="text-center">Particular Extra Entry</h4> {!!Form::open(['url'=>'/store-extraInfo','method'=>'POST' ,'class'=>'form-horizontal','enctype' => 'multipart/form-data'])!!}
            <div class="form-row">

                <div class="col-md-4">
                    <label for="dictionary_id">Dictionary</label>
                    <select class="custom-select form-control" name="dictionary_id"  id="dictionary_id" required>
                        <option value="">Select Dictionary</option>
                        @foreach($dictionarys as $dictionary)
                        <option value="{{$dictionary->id}}">{{$dictionary->dictionary_name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-4">
                    <label for="word_temp_id"> Status:</label>

                    <select class="custom-select form-control" name="word_temp_id"  id="status" required>
                        <option value="">Select Status</option>
                        <option value="1">New</option>                       
                        <option value="3">To Review</option>

                    </select>
                </div>
                <div class="col-md-4">
                    <label for="dictionaryWord">Word Name:</label>

                    <select class="custom-select form-control" name="word_temp_id" id="word_temp_id"  required>
                        <option value="" >Select Word</option>
                     
                    </select>
                </div>

            </div>
            
            
            <br>
            <div class="form-group">
                <label for="imageUrl">Image</label>
                <input type="file" class="form-control-file" name="image_url" id="imageUrl">
                <div id="image_url"></div>
                <!-- <img src=""  alt=""> -->
            </div>

            <div class="form-group">
                <label for="txtCulture">Culture</label>
                <textarea class="form-control text-area" id="txtCulture" name="in_culture" style="height:auto;"></textarea>
            </div>
            <div class="form-group">
                <label for="txtMedical">Medical</label>
                <textarea class="form-control" id="txtMedical" name="in_medical" style="height:auto;"></textarea>
               
            </div>
            <div class="form-group">
                <label for="txtBiology">Biology</label>
                <textarea class="form-control" id="txtBiology" name="in_biology" style="height:auto;"></textarea>
            </div>
            <div class="form-group">
                <label for="txtScience">Science</label>
                <textarea class="form-control" id="txtScience" name="in_science" style="height:auto;"></textarea>
            </div>
            <div class="form-group">
                <label for="txtEngineering">Engineering</label>
                <textarea class="form-control"   id="txtEngineering" name="in_engineering" style="height:auto;"></textarea>
            </div>

            <div class="form-group">
                <label for="txtHistory">History</label>
                <textarea class="form-control" id="txtHistory" name="history" style="height:auto;"></textarea>
            </div>
            <div class="ingineering">
           
            </div>
            <div id="Medical"> </div>
            <input type="submit" class="btn btn-primary" ></input>
            {!!Form::close()!!}

        </div>

    </div>
<script>
$(document).ready(function(){    
    function loadWord(dictionaryId,status){
        if(dictionaryId==null) return;
        if(status==null) return;
    $.ajax({
                    url: "{{url('/search/word/')}}/"+dictionaryId+"/"+status+"/",
                    type: "GET",      
                    success:function(data) {                       
                    $('#word_temp_id').empty();
                    $.each(data, function(key, value) {
                        $('#word_temp_id').append('<option value="'+ value.id +'">'+value.dictionary_word +'</option>');
                        });
                    }
                });
}      

    $('#dictionary_id').change(function(){
    var dictionaryId=$('#dictionary_id').val();
    var status=$('#status').val();
    if(dictionaryId>0 && status>0){
        loadWord(dictionaryId,status);
     }
 });

 $('#status').change(function(){
var dictionaryId=$('#dictionary_id').val();
var status=$('#status').val();
if(dictionaryId>0 && status>0){
    loadWord(dictionaryId, status);
}
});

    $('#word_temp_id').change(function(){
        var dictionaryId=$('#dictionary_id').val();
        var status=$('#status').val();
        var id=$(this).val();
    if(status==3)
    {
    alert(id);
    $.ajax({        
            url: "{{url('/particular-extrainfo')}}/"+dictionaryId+"/"+status+"/"+id+"/",
            type: "GET",                       
            dataType: "json",
            success: function (data) {                
                if(data)
                {                     
                $('#image_url').html('<img src="' + data.image_url + '"width=160 height=160/>'); 
                    $('#txtCulture').data("wysihtml5").editor.setValue(data.in_culture);
                    $('#txtMedical').data("wysihtml5").editor.setValue(data.in_medical);
                    $('#txtScience').data("wysihtml5").editor.setValue(data.in_science);
                    $('#txtBiology').data("wysihtml5").editor.setValue(data.in_biology);
                    $('#txtScience').data("wysihtml5").editor.setValue(data.in_science);
                    $('#txtEngineering').data("wysihtml5").editor.setValue(data.in_engineering);
                    $('#txtHistory').data("wysihtml5").editor.setValue(data.history);        
                }               
                
                    },
        });      

    }else{
        // alert('Select Status');
    }
    });
});
</script>

@endsection