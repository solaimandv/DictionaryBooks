@extends('admin.master')
@section('title')
Word Entry
@endsection
@section('content')

<div class="container-fluid">

        <hr>
        <div class="text-danger" id="lblMsg">
            {{Session::get("message")}}
        </div>
        <h3>
        </h3>
    <div class="col-lg-4 well">
    <h4 class="text-center">Word Temp Entry</h4>
    {!!Form::open(['url'=>'/store/wordtemp','method'=>'POST' ,'class'=>'form-horizontal'])!!}
        <div class="form-group">
        <label for="dictionary_id">Dictionary</label>
            <select class="custom-select form-control" name="dictionary_id" id="dictionary_id"  required>
                <option value="">Select Dictionary</option>
                @foreach($dictionarys as $dictionary)
                <option value="{{$dictionary->id}}">{{$dictionary->dictionary_name}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
           <label for="dictionaryWord">Word Name:</label>
           
           <input type="text" class="form-control" id="dictionary_word" name="dictionary_word" aria-describedby="emailHelp" placeholder="Word Name......" required>
        </div>
        <div class="form-group">
        <label for="dictionary_id">Group</label>
            <select class="custom-select form-control" name="group_id" id="group_id">
                <option value="">Select Group</option>
                @foreach($groups as $group)
                <option value="{{$group->id}}">{{$group->group_name}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
           <label for="type">Word Type:</label>
           <select class="custom-select form-control" name="type" id="type" required>
                <option value="">Select Type</option>
                
                <option value="Noun">Noun</option>
                <option value="Pronoun">Pronoun</option>
                <option value="Adjective">Adjective</option>
                <option value="Verb">Verb</option>
                <option value="Adverb">Adverb</option>
                <option value="Preposition">Preposition</option>
                <option value="Conjunction">Conjunction</option>
                <option value="interjunction">interjunction</option>
                
            </select>
        </div>
        
           
           <input type="hidden" value="defaultName" class="form-control" id="create_by" name="create_by" aria-describedby="emailHelp" placeholder="Word Name......"required>
           
           <input type="hidden" value="1" class="form-control" id="status" name="status" aria-describedby="emailHelp" placeholder="Word Name......"required>
     
           <input type="hidden" value="2017/10/10" class="form-control" id="import_date" name="import_date" aria-describedby="emailHelp" placeholder="Word Name......" required>
         
           <input type="hidden" value="1" class="form-control" id="is_imported" name="is_imported" aria-describedby="emailHelp" placeholder="Word Name......" required>
        
           <input type="hidden" value="defaultName" class="form-control" id="import_by" name="import_by" aria-describedby="emailHelp" placeholder="Word Name......"required>
        
     <input type="submit" class="btn btn-primary"></input>
     {!!Form::close()!!}
       
 </div>
    

    <div class="col-lg-8">
    <div class="panel panel-default">
                        <div class="panel-heading text-center">
                             Word Temp List
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="generalData">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Action</th>
                                            <th>Dictionary</th>
                                            <th>Word</th>
                                            <th>Word Type</th>
                                           
                                        </tr>
                                    </thead>
                                    <tbody>                                    
                                    <?php
                                    $i=1; 
                                    foreach ($wordtemps as $wordtemp) {?>
                                    <tr class="" id="generalWord">
                                          <td><?php echo $i++ ?></td>
                                          <td>
                                          <button type="button">
                                          <a href="{{url('/edit-wordstemp/'.$wordtemp->id)}}"><i class="fa fa-pencil-square"  aria-hidden="true" title="Edit"></i></a></button>
                                          <button type="button">
                                         <a href="{{url('/delete-wordstemp/'.$wordtemp->id)}}"><i class="fa fa-trash" aria-hidden="true" title="Delete" ></i></a></button>
                                        
                                          </td>
                                            <td><?php echo $wordtemp->dictionary_name ?></td>
                                            <td><?php echo ucfirst($wordtemp->dictionary_word) ?></td>
                                            <td><?php echo $wordtemp->type ?></td>                                      
                                       
                                      </tr>                                     
                                     
                                        <?php }?>
                                       
                                    </tbody>
                                </table>

                                <table class="table table-striped table-bordered table-hover" id="ajaxData" style="display:none">
                                    <thead>
                                        <tr>
                                            
                                            
                                            <th>Dictionary</th>
                                            <th>Word</th>
                                            <th>Word Type</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody id="success">                                  
                                   
                                       
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
    </div>
   
</div>
<script type="text/javascript">
 $('#dictionary_id').change(function () {
    $('#lblMsg').text("");
 });
 $('#dictionary_id').change(function () {
    $('#lblMsg').text("");
 });
 $('#dictionary_id').change(function () {
    $('#lblMsg').text("");
 });






</script>

<script>
$(document).ready(function(){

    $('#dictionary_id').change(function () {
var dictionary=$(this).val();
// alert(dictionary);
if(dictionary)
{
    $('#generalData').hide();
    $('#ajaxData').show();
}else{
    $('#generalData').show();
    $('#ajaxData').hide(); 
}
$.ajax({
    type: "GET",
    url:"{{url('/word/by-dictionary')}}/"+dictionary+"/",   
    dataType: "json",  
   
}) .done(function(data){
		
// console.log(data);
var	rows = '';
	$.each( data, function( key, value ) {
	  	rows = rows + '<tr>';
	  	rows = rows + '<td>'+value.dictionary_name+'</td>';
	  	rows = rows + '<td>'+value.dictionary_word+'</td>';
	  	rows = rows + '<td>'+value.type+'</td>';
	  	rows = rows + '<td data-id="'+value.id+'">';
                // rows = rows + '<button data-toggle="modal" data-target="#edit-item" class="btn btn-primary edit-item">Edit</button> ';
                rows = rows + '<button class="btn btn-danger remove-item">Delete</button>';
                rows = rows + '</td>';
	  	rows = rows + '</tr>';
	});
	$("tbody").html(rows);
	});
    $("body").on("click",".remove-item",function(){
    var id = $(this).parent("td").data('id');
    var c_obj = $(this).parents("tr");
    confirm("Are you sure to delete this.");
    $.ajax({
        dataType: 'json',
        type:'GET',
        url: "{{url('/delete-wordstemp')}}/"+id+"/",
    }).done(function(data){
        if(data.success == true){ // if true (1)
      setTimeout(function(){// wait for 5 secs(2)
           location.reload(); // then reload the page.(3)
      }, 5000); 
   }
    });
});

 });

});


</script>
@endsection
