@extends('admin.master')
@section('title')
Word Entry
@endsection
@section('content')

<div class="container-fluid">
    <div class="row bg-success">
       
        <div class="col-lg-3 ">
            <div class="form-group">
                <select class="form-control">
                    <option selected> Select Dictionry</option>
                    @foreach($dictionarys as $data)
                    <option value="{{$data->dictionary_id}}">{{$data->dictionary_name}}</option>
                   @endforeach
                </select>
            </div>
        </div>
        <div class="col-lg-2">
            <div class="form-group">
                <select class="form-control">
                    <option selected> Select Type</option>
                    <option value="1">One</option>
                    <option value="2">Two</option>
                    <option value="3">Three</option>
                </select>
            </div>
        </div> 
        
        <div class="col-lg-7">
        <form class="form-inline">
  
            <div class="form-group ">
                <label for="search" class="sr-only"></label> Search
                <input type="search" onkeyup="wsearch();" class="form-control" id="search"  placeholder="Search Word">
            </div>
            <input type="submit" class="btn btn-primary">
         </form>
        </div>
    </div>
        
    <div class="row">
    <div class="col-lg-4">
    <div class="panel panel-default">
                        <div class="panel-heading">
                            Word List
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover generaldata" >
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Action</th>
                                            <th>Word Name</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody id="">
                                    <?php $i=1?>
                                    @foreach($objWord as $data)
                                        <tr>
                                            <td>{{$i}}</td>
                                            <td>
                                            <button type="button">
                                        <a href="{{url('/view-details/'.$data->id)}}"><i class="fa fa-pencil-square"  aria-hidden="true" title="Edit"></i></a></button>
                                        <button type="button" >
                                        <a href=""><i class="fa fa-trash" aria-hidden="true" title="Delete" ></i></a></button>
                                            </td>
                                            <td>{{$data->dictionary_word}}</td>
                                           
                                        </tr>
                                        <?php $i++ ?>
                                     @endforeach
                                    </tbody>
                                    
                                </table>
                                {{ $objWord->links() }}
                                <table class="table table-striped table-bordered table-hover ajaxdata " style="display:none" >
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Action</th>
                                            <th>Word Name</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody id="success">
                                    
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
    </div>
    <div class="col-lg-8">
    <div class="panel panel-default">
                        <div class="panel-heading text-center">
                            Particulars
                        </div>
                        <div class="panel-body">
                        <h3>{{$particulars->dictionary_word}} : {{$particulars->meaning}}</h3>
                         <img src="{{$particulars->meaning_image_url}}" alt="">
                         <img src="{{$particulars->audio_url}}" alt="">
                          <hr>  
                            <h4>Noun:</h4>
                            <p>{!!$particulars->noun!!}</p>
                        <hr>
                            <h4>Verb:</h4>
                            <p>{!!$particulars->verb!!}</p>
                        <hr>
                            <h4>Pronoun:</h4>
                            <p>{!!$particulars->pronoun!!}</p>
                        <hr>
                            <h4>Adjective:</h4>
                            <p>{!!$particulars->adjective!!}</p>
                        <hr>
                            <h4>Adverb:</h4>
                            <p>{!!$particulars->adverb!!}</p>
                        <hr>
                            <h4>Singular:</h4>
                            <p>{!!$particulars->singular!!}</p>
                        <hr>
                            <h4>Plural:</h4>
                            <p>{!!$particulars->plural!!}</p>
                        <hr>
                            <h4>Synonyms:</h4>
                            <p>{!!$particulars->synonym!!}</p>
                        <hr>
                            <h4>Antonym:</h4>
                            <p>{!!$particulars->antonym!!}</p>
                        <hr>
                            <h4>Origin:</h4>
                            <p>{!!$particulars->origin!!}</p>
                      
                         
                        </div>
                        <div class="panel-footer">
                        <div class="center"></div>
                        <input class="btn btn-primary btn-lg"  type="submit" value="Publish">
                        <input class="btn btn-success btn-lg" type="submit" value="Review">
                        </div>
    
    </div>
    
    </div>

</div>

@endsection
