@extends('admin.master')
@section('title')
Word Edit
@endsection
@section('content')

<div class="container-fluid">

        <hr>
        <h3>
        {{Session::get("message")}}
        </h3>
    <div class="col-lg-4 well">
    <h4 class="text-center">Word Temp Edit</h4>
    {!!Form::open(['url'=>'/update-wordtemp','method'=>'POST' ,'class'=>'form-horizontal','name'=>'editwordform'])!!}
        <div class="form-group">
        <label for="dictionary_id">Dictionary</label>
            <select class="custom-select form-control" name="dictionary_id" id="dictionary_id"  required>
                <option value="">Select Dictionary</option>
                @foreach($dictionarys as $dictionary)
                <option value="{{$dictionary->id}}">{{$dictionary->dictionary_name}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
           <label for="dictionaryWord">Word Name:</label>
           <input type="text" class="form-control" id="dictionary_word" name="dictionary_word" value="{{$objWord->dictionary_word}}" aria-describedby="emailHelp" required>
           <input type="hidden" class="form-control" id="dictionaryWord" name="id" value="{{$objWord->id}}" aria-describedby="emailHelp" required>
        </div>
        <div class="form-group">
        <label for="dictionary_id">Group</label>
            <select class="custom-select form-control" name="group_id" id="group_id"  required>
                <option value="">Select Group</option>
                @foreach($groups as $group)
                <option value="{{$group->id}}">{{$group->group_name}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
           <label for="type">Word Type:</label>
           <select class="custom-select form-control" name="type" id="type" required>
                <option value="{{$objWord->type}}">{{$objWord->type}}</option>
                
                <option value="Noun">Noun</option>
                <option value="Pronoun">Pronoun</option>
                <option value="Adjective">Adjective</option>
                <option value="Verb">Verb</option>
                <option value="Adverb">Adverb</option>
                <option value="Preposition">Preposition</option>
                <option value="Conjunction">Conjunction</option>
                <option value="interjunction">interjunction</option>
                
            </select>
        </div>
        
           
           <input type="hidden" value="defaultName" class="form-control" id="create_by" name="create_by" aria-describedby="emailHelp" placeholder="Word Name......"required>
           <input type="hidden" value="1" class="form-control" id="status" name="status" aria-describedby="emailHelp" placeholder="Word Name......"required>
     
        
         
           <input type="hidden" value="2017/10/10" class="form-control" id="import_date" name="import_date" aria-describedby="emailHelp" placeholder="Word Name......" required>
        
        
           
           <input type="hidden" value="1" class="form-control" id="is_imported" name="is_imported" aria-describedby="emailHelp" placeholder="Word Name......" required>
        
        
           
           <input type="hidden" value="defaultName" class="form-control" id="import_by" name="import_by" aria-describedby="emailHelp" placeholder="Word Name......"required>
        
     <input type="submit" class="btn btn-primary"></input>
     {!!Form::close()!!}
       
 </div>
    

    <div class="col-lg-8">
    <div class="panel panel-default">
                        <div class="panel-heading text-center">
                             Word Temp List
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive" id="table">
                                <table class="table table-striped table-hover" >
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Action</th>
                                            <th>Dictionary</th>
                                            <th>Word Name</th>
                                            <th>Word Type</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                  <?php $i=1?>
                                  @foreach($wordtemps as $wordtemp)
                                  <tr class="">
                                        <td>{{$i++}}</td>
                                        <td>
                                        <button type="button">
                                        <a href="{{url('/edit-wordstemp/'.$wordtemp->id)}}"><i class="fa fa-pencil-square"  aria-hidden="true" title="Edit"></i></a></button>
                                        <button type="button" onclick="return confirm('Are you sure to delete the record?');">
                                        <a href="{{url('/delete-wordstemp/'.$wordtemp->id)}}"><i class="fa fa-trash" aria-hidden="true" title="Delete" ></i></a></button>
                                        </td>
                                        <td>{{$wordtemp->dictionary_name}}</td>
                                        <td>{{$wordtemp->dictionary_word}}</td>
                                        <td>{{$wordtemp->type}}</td>
                                       
                                       
                                    </tr>
                                    
                                    
                                  @endforeach
                                  
                                    </tbody>
                                    
                                </table>
                                {{$wordtemps->links()}} 
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
    </div>
   
</div>
<script type="text/javascript">
document.forms['editwordform'].elements['dictionary_id'].value={{$objWord->dictionary_id}}
document.forms['editwordform'].elements['type'].value={{$objWord->type}}
</script>
<script>
// $.(document).on('click','.pagination a', function(e){
// e.preventDefault();
// console.log("solaiman");
// });
</script>


@endsection
