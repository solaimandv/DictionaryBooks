

@extends('admin.master')
@section('title')
Review Word
@endsection
@section('content')


<div ng-app="app" ng-controller="ReviewController">
    <div class="container-fluid">
        <h3 class="text-center"> Review Word</h3>
        <div class="row well">
            <form class="form-inline">
                <div class="form-group">
                    <label for="cmbdictionary">Dictionary</label>
                    <select class="form-control" ng-model="DictionaryId" ng-options="dic.id as dic.dictionary_name for dic in Dictionaries"></select>
                </div>
            
                <div class="form-group">
                    <select class="form-control" ng-model="PublishStatus" id="PublishStatus" required>
                    <option value="">Select Status</option>
                        <option value="1">New</option>
                        <option value="2">Imported</option>
                        <option value="4">Reviewed</option>
                    </select>
                </div>
                <div class="form-group ">
                    <label for="search" class="sr-only"></label> Search
                    <input type="search" class="form-control" id="search" ng-model="SearchText" placeholder="Search Word">
                </div>
                <button class="btn btn-search" id="button" ng-click="Search(DictionaryId,PublishStatus,SearchText);" type="button"><i class="fa fa-search fa-fw"></i> Search</button>

            </form>
        </div>

        <div class="row well">
            <div class="col-lg-4">

                <div class="table-responsive">
                    <div class="panel panel-default">
                        <table class="table  table-striped" id="ItemListTable">
                            <thead>
                                <tr>
                                    <th>SN#</th>
                                    <th>Word Name</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                <tr ng-repeat="word in WordTemps">
                                    <td>// $index+1 //</td>
                                    <td>// word.dictionary_word //</td>
                                    <td><button class="btn btn-primary" ng-click="ShowParticular(word)" type="button">Particulars</button></td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                    <div class="alert alert-default" ng-show="showNotFound" style="background-color: #fcf8e3">
                        <strong>No data found</strong>
                    </div>
                </div>
            </div>
            <div class="col-lg-8 bg-red">
                <div class="panel panel-default">
                    <div class="panel-heading text-center">
                                Particulars
                    </div>
                    <div class="panel-body">
                        
                           <h4> Meaning : // Particular.meaning //</h4>
                           <h4> Image : </h4>
                           <img src=" // Particular.meaning_image_url //" alt="" style="width:300px; height:400px">
                           <h4> Audio :</h4>
                           <div id="music"><embed src="// Particular.audio_url //" autostart=true loop=false></div> 
                           <!-- <audio src="// Particular.audio_url //"></audio>   -->
                        <hr>
                        

                        <h4> Noun :</h4>                       
                        <p ng-bind-html="Particular.noun"></p>
                        <hr>

                        <h4> Pronoun :</h4>                       
                        <p ng-bind-html="Particular.pronoun"></p>
                        <hr>

                        <h4> Verb :</h4>                        
                         <p ng-bind-html="Particular.verb"></p>
                        <hr>

                        <h4> Adverb :</h4>                        
                        <p ng-bind-html="Particular.adverb"></p>
                        <hr>

                        <h4> Adjective :</h4>                      
                        <p ng-bind-html="Particular.adjective"></p>
                        <hr>                        

                        <h4> Sigular :</h4>
                        <h4>  // Particular.singular //</h4>
                        <p ng-bind-html="Particular.singular"></p>
                        <hr>

                        <h4> Plural :</h4>
                        <p ng-bind-html="Particular.plural"></p>
                        <hr>

                        <h4> Synonyms :</h4>                       
                        <p ng-bind-html="Particular.synonym"></p>
                        <hr>

                        <h4> Antonym :</h4>
                        <p ng-bind-html="Particular.antonym"></p>
                                             
                        <hr> 
                        <h4> Image : </h4>
                           <img src=" // Particular.image_url //" alt="" style="width:300px; height:400px">
                           <hr>                      
                        <h4> Science :</h4>                       
                        <p ng-bind-html="Particular.in_science"></p>
                        <hr>
                        <h4> Medical :</h4>
                        <p ng-bind-html="Particular.in_medical"></p>
                        <hr>
                        <h4>Biology  :</h4>
                        <p ng-bind-html="Particular.in_biology"></p>                        
                        <hr>
                        <h4>Engineering :</h4>
                        <p ng-bind-html="Particular.in_engineering"></p>                          
                        <hr>
                        <h4>Culture :</h4>                       
                        <p ng-bind-html="Particular.in_culture"></p>
                        <hr>
                        <h4> Origin :</h4>
                        <p ng-bind-html="Particular.origin"></p>                      
                        <hr>
                        <h4>History :</h4>                        
                        <p ng-bind-html="Particular.history"></p>  
                        
                       
                    </div>
                    <div class="panel-footer">
                            <div class="center"></div>
                            <input class="btn btn-primary btn-lg" type="submit" id="Publish" ng-click="SaveParticular(Particular)" value="Publish">
                            <input class="btn btn-success btn-lg" type="submit" id="Review"  ng-click="ReviewParticular(Particular)" value="Review">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{asset('public/vendor/MainJS/WordReview.js')}}"></script>
<script>
$(document).ready(function(){
    $("button").click(function(){
        var PublishStatus=$('#PublishStatus').val();
if(PublishStatus==1)
{
    $("#Publish").show();
    $("#Review").hide();
    
}else if(PublishStatus==2)
{
    $("#Publish").hide();
    $("#Review").show();
}else if(PublishStatus==4)
{
    $("#Publish").show();
    $("#Review").hide();
}
    });

});

</script>
@endsection
